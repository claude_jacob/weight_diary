const getWeeksBetween = (date1, date2) => {
    var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();
    var difference_ms = Math.abs(date1_ms - date2_ms);
    return Math.floor(difference_ms / ONE_WEEK);
}

let d = new Date().toDateString()
console.log(getWeeksBetween(new Date(), new Date('March 19, 2019')))