import calcBmi from 'bmi-calc'

export const getBMI = (weight, height, level, kcal) => {
    let bmi = calcBmi(parseFloat(weight), (height / 100.0))
    if (level) {
        let calorie = weight * level
        bmi.calorie = calorie
        if (parseFloat(bmi.value) < 18.5) {
            bmi.message = `
        Your BMI is ${parseFloat(bmi.value).toFixed(3)} which is equivalent to ${bmi.name}. Your regular calorie must be ${calorie}, your current calorie intake is ${kcal} you may ${kcal > calorie ? 'gain' : 'lose'} weight.
        If you want to gain weight your kcal must be ${calorie + 200} kcal per day and do 20 - 30 minutes exercises.
        `
        } else if (parseFloat(bmi.value) >= 18.5 && parseFloat(bmi.value) <= 24.9) {
            bmi.message = `
        Your BMI is ${parseFloat(bmi.value).toFixed(3)} which is equivalent to ${bmi.name}. Your regular calorie must be ${calorie}, your current calorie intake is ${kcal} you may ${kcal > calorie ? 'gain' : 'lose'} weight.
        To maintain weight use ${calorie} kcal per day and do 20 - 30 minutes exercises.
        `
        } else if (parseFloat(bmi.value) >= 25 && parseFloat(bmi.value) <= 29.9) {
            bmi.message = `
        Your BMI is ${parseFloat(bmi.value).toFixed(3)} which is equivalent to ${bmi.name}. Your regular calorie must be ${calorie}, your current calorie intake is ${kcal} you may ${kcal > calorie ? 'gain' : 'lose'} weight.
        If you want to lose weight your kcal must be 1000 per day and do 30 - 40 minutes exercises.
        `
        } else if (parseFloat(bmi.value) >= 30 && parseFloat(bmi.value) <= 35) {
            bmi.message = `
        Your BMI is ${parseFloat(bmi.value).toFixed(3)} which is equivalent to ${bmi.name}. Your regular calorie must be ${calorie}, your current calorie intake is ${kcal} you may ${kcal > calorie ? 'gain' : 'lose'} weight.
        If you want to lose weight your kcal must be 1000 per day and do 30 - 40 minutes exercises.
        `
        } else if (parseFloat(bmi.value) > 35) {
            bmi.message = `
        Your BMI is ${parseFloat(bmi.value).toFixed(3)} which is equivalent to ${bmi.name}. Your regular calorie must be ${calorie}, your current calorie intake is ${kcal} you may ${kcal > calorie ? 'gain' : 'lose'} weight.
         If you want to lose weight your kcal must be 1000 per day and do 30 - 40 minutes exercises.
        `
        }
    }
    return bmi
}