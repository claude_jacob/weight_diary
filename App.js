import React from 'react';
import { persistor, store } from './store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/lib/integration/react'
import Screens from './components/Screens'

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Screens />
        </PersistGate>
      </Provider>
    );
  }
}

console.ignoredYellowBox = true