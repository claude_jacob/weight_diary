import React from 'react'
import { View, Picker, TouchableOpacity, Modal, TouchableHighlight, ScrollView, Alert, ImageBackground, Dimensions } from 'react-native'
import { Text, ListItem, Input, Icon } from 'react-native-elements'
import { connect } from 'react-redux'
import appFiles from '../assets/appFiles'
import { robotoWeights } from 'react-native-typography'
const { height } = Dimensions.get("screen")
import { addToIntake, addUserCalorie, resetCalorie, setUserCalorie, updateLastCalorieDate, updateTotal, resetTotal } from '../actions/appActions'
import { Actions } from 'react-native-router-flux';
class Food extends React.Component {

    state = {
        searchText: '',
        consumedList: [],
        result: [],
        showList: false,
        modalVisible: false,
        currentFood: null,
        quantity: '1',
        grams: '0',
        page: '0',
        total: {},
        calculateClicked: false
    }

    clearFood = () => {
        this.setState({
            searchText: '',
            consumedList: [],
            showList: false,
            modalVisible: false,
            currentFood: null,
            quantity: '1',
            grams: '0',
            total: {}
        })
        this.props.resetTotal()
    }

    getQuantities = () => {
        let quantities = [
            {
                value: '1/4'
            },
            {
                value: '1/2'
            },
            {
                value: '3/4'
            },
        ]
        for (let x = 1; x <= 25; x++) {
            quantities.push({ value: `${x}` })
        }
        return quantities
    }

    showListButton = () => this.setState({ showList: true })

    hideListButton = () => this.setState({ showList: false })

    handleInputChange = text => {
        this.setState({ searchText: text })
    }

    handleSearch = () => {
        let keys = Object.keys(appFiles.foods)

        let arr = []
        for (let key of keys) {
            for (let o of appFiles.foods[key]) {
                arr.push(o)
            }
        }

        const newData = arr.filter(item => {
            return item.name.toLowerCase().indexOf(this.state.searchText.toLowerCase()) > -1
        })
        this.setState({ result: newData })
    }

    componentDidMount() {
        this.updateList()
        this.setState({ total: this.props.total })
    }

    updateList = () => {
        this.setState({ result: appFiles.foods[this.state.page] })
    }

    addFood = food => {
        this.setModalVisible(true)
        this.setState({ currentFood: food })

    }

    removeFood = index => {
        this.setState({
            consumedList: [
                ...this.state.consumedList.slice(0, index),
                ...this.state.consumedList.slice(index + 1)
            ],
        })
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    getNumber = num => isNaN(eval(num)) ? 1 : parseFloat(eval(num))

    getTotalFoodDetails = () => {
        if (!this.state.calculateClicked) {
            let total = this.state.total
            for (let food of this.state.consumedList) {
                total.carbohydrate += (food.carbohydrate * this.getNumber(food.quantity))
                total.protein += (food.protein * this.getNumber(food.quantity))
                total.fat += (food.fat * this.getNumber(food.quantity))
                total.kilocalories += (
                    (food.carbohydrate * this.getNumber(food.quantity) * 4) + 
                    (food.protein * this.getNumber(food.quantity) * 4) + 
                    (food.fat * this.getNumber(food.quantity) * 9)
                )
                total.kilojoules += (food.kilojoules * this.getNumber(food.quantity))
            }

            this.setState({ total: total, calculateClicked: true })
        }
    }

    isEmpty = obj => Object.keys(obj).length === 0 && obj.constructor === Object

    saveFoodToIntakes = () => {
        this.props.addToIntake({
            date: new Date(),
            message: `
            YOU HAVE CONSUMED:\n
            \t${this.props.total.carbohydrate} g carbohydrate\n
            \t${this.props.total.fat} g fat\n
            \t${this.props.total.kilocalories} g kilocalories\n
            \t${this.props.total.kilojoules} g kilojoules\n
            \t${this.props.total.protein} g protein
            `
        })

        if (new Date(this.props.lastCalorieUpdate).toDateString() === new Date().toDateString()) {
            this.props.setUserCalorie(parseFloat(parseFloat(this.props.total.kilocalories)))
        } else {
            this.props.addUserCalorie(parseFloat(this.props.total.kilocalories))
        }

        this.props.updateLastCalorieDate(new Date())
        this.clearFood()
        Actions.statistics()
    }

    render() {
        return (
            <ImageBackground style={{ height: '100%', width: '100%' }} source={require('../assets/background.png')}>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}>
                    <View style={{ margin: 22 }}>
                        <View style={{ alignItems: 'center', marginTop: 50 }}>
                            <Text style={{ ...robotoWeights.light, fontSize: 20 }}>Quantity for {this.state.currentFood ? this.state.currentFood.name : ''}</Text>
                            <Picker
                                selectedValue={this.state.quantity}
                                style={{ height: 50, width: 100 }}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({ quantity: itemValue, currentFood: { ...this.state.currentFood, quantity: itemValue }, calculateClicked: false })
                                }>
                                {
                                    this.getQuantities().map((q, i) => <Picker.Item key={i + 'c'} label={q.value} value={q.value} />)
                                }
                            </Picker>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.setModalVisible(false)} style={{ margin: 5, height: 40, width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                                    <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)' }}>CANCEL</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => {
                                    this.setModalVisible(false);
                                    this.setState({ consumedList: this.state.consumedList.concat(this.state.currentFood), currentFood: null, quantity: '1' })
                                }} style={{ margin: 5, height: 40, width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                                    <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)' }}>SUBMIT</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
                <View style={{ alignContent: 'center', alignItems: 'center', marginTop: 23 }}>
                    <View>
                        <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>Current Weight: {this.props.weight} kg</Text>
                    </View>
                    {!this.state.showList &&
                        <View style={{ alignContent: 'center', alignItems: 'center' }}>
                            <Input value={this.state.searchText} placeholder='Enter Food' placeholderTextColor={'rgba(206, 201, 233,1)'}
                                rightIcon={{
                                    name: 'search', color: 'rgba(206, 201, 233,1)', onPress: this.handleSearch
                                }} inputStyle={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)' }} containerStyle={{ backgroundColor: 'rgba(0,0,0, 0.5)', borderRadius: 5, margin: 15, width: 300 }} onChangeText={this.handleInputChange} />
                            <View style={{ width: '100%', marginTop: 10 }}>
                                <ScrollView style={{ height: height * .6 }}>
                                    {
                                        this.state.result.map((item, index) => (
                                            <ListItem key={index + 'a'}
                                                containerStyle={{ backgroundColor: 'transparent' }}
                                                onPress={() => this.addFood(item)}
                                                leftElement={<Text style={{ color: 'rgba(206, 201, 233,1)' }}>{item.name}</Text>}
                                                rightElement={<Text style={{ color: 'rgba(206, 201, 233,1)' }}>{item.serving}</Text>} />
                                        ))
                                    }
                                    <ListItem titleStyle={{ ...robotoWeights.light, color: 'white', textAlign: 'center' }} title={`Page ${this.state.page} of 24`} containerStyle={{ backgroundColor: 'transparent' }} rightIcon={{
                                        name: 'chevron-right', color: 'white', size: 35,
                                        onPress: () => {
                                            this.setState({ page: `${parseInt(this.state.page) < 24 ? parseInt(this.state.page) + 1 : 0}` }, this.updateList)
                                        }
                                    }} leftIcon={{
                                        name: 'chevron-left', color: 'white', size: 35,
                                        onPress: () => {
                                            this.setState({ page: `${parseInt(this.state.page) > 0 ? parseInt(this.state.page) - 1 : 24}` }, this.updateList)
                                        }
                                    }} />
                                </ScrollView>
                            </View>
                            <TouchableOpacity onPress={this.showListButton} style={{ margin: 5, height: 40, width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                                <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)' }}>FOOD LIST</Text>
                            </TouchableOpacity>
                        </View>
                    }

                    {
                        this.state.showList &&
                        <View style={{ alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 15 }}>{this.state.total.carbohydrate} g carbohydrate</Text>
                            <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 15 }}>{this.state.total.fat} g fat</Text>
                            <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 15 }}>{this.state.total.kilocalories} g kilocalories</Text>
                            <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 15 }}>{this.state.total.kilojoules} g kilojoules</Text>
                            <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 15 }}>{this.state.total.protein} g protein</Text>
                            <View style={{ width: '100%', marginTop: 10 }}>
                                <ScrollView style={{ height: height * .45 }}>
                                    {
                                        this.state.consumedList.map((item, index) => (
                                            <ListItem key={index + 'c'}
                                                containerStyle={{ backgroundColor: 'transparent' }}
                                                onPress={() => Alert.alert('Remove Item', 'Are you sure to remove this food?', [
                                                    { text: 'Yes', onPress: () => this.removeFood(index) },
                                                    { text: 'No', onPress: () => { } }
                                                ])}
                                                leftElement={<Text style={{ color: 'rgba(206, 201, 233,1)' }}>{item.name}</Text>}
                                                rightElement={<Text style={{ color: 'rgba(206, 201, 233,1)' }}>{this.getNumber(item.quantity)} serving/s</Text>} />
                                        ))
                                    }
                                    {
                                        this.state.consumedList.length === 0 && <View>
                                            <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)' }}>There are no food selected</Text>
                                        </View>
                                    }
                                </ScrollView>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={this.hideListButton} style={{ margin: 5, height: 40, width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                                    <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)' }}>SELECT MORE FOOD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.getTotalFoodDetails} style={{ margin: 5, height: 40, width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                                    <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)' }}>CALCULATE</Text>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity onPress={this.saveFoodToIntakes} style={{ margin: 5, height: 40, width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                                <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)' }}>SAVE</Text>
                            </TouchableOpacity>
                        </View>
                    }
                </View>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    weight: state.app.user.weight,
    foods: state.app.foods,
    lastCalorieUpdate: state.app.lastCalorieUpdate,
    calories: state.app.calories,
    total: state.app.total,
    date: state.app.date
})

const mapDispatchToProps = {
    addToIntake, addUserCalorie, updateLastCalorieDate, updateTotal, setUserCalorie, resetCalorie, resetTotal
}

export default connect(mapStateToProps, mapDispatchToProps)(Food)