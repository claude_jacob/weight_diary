import React from 'react'
import { View, ImageBackground, ScrollView, Alert } from 'react-native'
import { Text, ListItem } from 'react-native-elements'
import { connect } from 'react-redux'
import { robotoWeights } from 'react-native-typography'

class Adequate extends React.Component {

    getAdequateAverage = () => {
        let calories = 0
        for (let i of this.props.calories) {
            calories += i
        }
        return this.props.user.bmi ? isNaN(((calories / 7) / this.props.user.bmi.calorie) * 100) ? 0  : ((calories / 7) / this.props.user.bmi.calorie) * 100 : 0
    }

    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ height: '100%', width: '100%' }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 28, textDecorationLine: 'underline' }}>Adequate Average</Text>
                    <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 25 }}>{this.getAdequateAverage().toFixed(3)}%</Text>
                </View>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    user: state.app.user,
    calories: state.app.calories
})

export default connect(mapStateToProps)(Adequate)