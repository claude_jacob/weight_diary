import React from 'react'
import Register from './Register'
import Statistics from './Statistics'
import Food from './Food'
import Exercise from './Exercise'
import WeightDiary from './WeightDiary'
import FoodIntake from './FoodIntake'
import BMI from './BMI'
import { Router, Scene } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { Icon } from 'react-native-elements'
import Adequate from './Adequate'

class Screens extends React.Component {

    componentDidMount(){
        console.log(this.props.app)
    }
    render() {
        return (
            <Router>
                <Scene key="root" headerMode="none">
                    <Scene key="register" activeTintColor="#3dbb2d" activeBackgroundColor="#3dbb2d" component={Register} initial={!this.props.user} />
                    <Scene tabs key="scenes" tabBarStyle={{ backgroundColor: 'rgba(55,52,71, 1)' }} initial={this.props.user !== null}>
                        <Scene key="statistics" title="Profile" icon={iconProfile} activeTintColor="#3dbb2d" activeBackgroundColor="#3dbb2d" component={Statistics} />
                        <Scene key="bmi" title="BMI" icon={iconBmi} activeTintColor="#3dbb2d" activeBackgroundColor="#3dbb2d" component={BMI} />
                        <Scene key="food" title="Food" icon={iconFood} activeTintColor="#3dbb2d" activeBackgroundColor="#3dbb2d" component={Food} />
                        <Scene key="intake" title="Intake" icon={iconFoodIntake} activeTintColor="#3dbb2d" activeBackgroundColor="#3dbb2d" component={FoodIntake} />
                        <Scene key="adequate" title="Adequate" icon={adequatePercentage} activeTintColor="#3dbb2d" activeBackgroundColor="#3dbb2d" component={Adequate} />
                        <Scene key="exercise" title="Exercise" icon={iconExercise} activeTintColor="#3dbb2d" activeBackgroundColor="#3dbb2d" component={Exercise} />
                        <Scene key="diary" title="Diary" icon={iconDiary} activeTintColor="#3dbb2d" activeBackgroundColor="#3dbb2d" component={WeightDiary} />
                    </Scene>
                </Scene>
            </Router>
        )
    }
}

const iconProfile = () => (
    <Icon name='account-circle' size={25} />
)

const iconFood = () => (
    <Icon name='local-dining' size={25} />
)

const iconFoodIntake = () => (
    <Icon name='local-drink' size={25} />
)

const adequatePercentage = () => (
    <Icon name='timeline' size={25} />
)

const iconBmi = () => (
    <Icon name='linear-scale' size={25} />
)

const iconExercise = () => (
    <Icon name='games' size={25} />
)

const iconDiary = () => (
    <Icon name='note' size={25} />
)



console.ignoredYellowBox = false
console.disableYellowBox = true

const mapStateToProps = state => ({
    user: state.app.user,
    app: state.app
})

export default connect(mapStateToProps)(Screens)