import React from 'react'
import { LineChart } from 'react-native-chart-kit'
import { Alert, TouchableOpacity, Dimensions, View, ImageBackground, ScrollView, Modal } from 'react-native'
import { Avatar, Text, Button, Icon } from 'react-native-elements'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import { robotoWeights } from 'react-native-typography'
import { updateLoading, setDate, resetCalories, resetTotal } from '../actions/appActions'
import DatePicker from 'react-native-datepicker'

class Statistics extends React.Component {
    state = {
        modalVisible: false,
        date: new Date(),
        calorieGraphVisible: false,
        dateVisible: false
    }

    getWeeksString = (weeks) => {
        let weeklist = []
        for (let x = 1; x <= weeks; x++) {
            weeklist.push(`${x}`)
        }
        return weeklist
    }

    getGraphData = () => {
        let labels = []
        let datasets = [
            {
                data: []
            },
            {
                data: []
            },
        ]
        for (let graph of this.props.graph) {
            datasets[0].data.push(graph.data)
            labels.push(graph.label)
        }
        for (let bmig of this.props.bmigraph) {
            datasets[1].data.push(bmig.data)
        }
        datasets[0].color = (opacity) => `rgba(0,0,0, ${1})`,
            datasets[1].color = (opacity) => `rgba(206, 201, 233, ${1})`

        return { labels: labels, datasets: datasets }
    }

    getCaloriGraphData = () => {
        let labels = []
        let reccomended = parseInt(this.props.user.bmi.calorie)

        let datasets = [
            {
                data: []
            },
            {
                data: []
            }
        ]

        for (let x = 0; x < this.props.calories.length; x++) {
            datasets[0].data.push(reccomended)
        }

        for (let calorie of this.props.calories) {
            datasets[1].data.push(calorie)
        }

        for (let y = 1; y <= this.props.calories.length; y++) {
            labels.push(`${y} day`)
        }
        datasets[0].color = (opacity) => 'rgba(46, 139, 87, 1)'
        datasets[1].color = opacity => 'rgba(206, 201, 233, 1)'

        return { labels: labels, datasets: datasets }
    }

    componentDidMount() {
        if (new Date(this.props.dateCreated).getDay() === new Date().getDay() && new Date(this.props.dateCreated).toDateString() !== new Date().toDateString()) {
            Alert.alert('Good News!', `Greetings ${this.props.name}, you can update your BMI and Weight graph today.`)
        }
    }

    setModalVisible = value => this.setState({ modalVisible: value })

    setCalorieGraphVisible = value => this.setState({ calorieGraphVisible: value })

    render() {
        const chartConfig = {
            backgroundGradientFrom: 'rgba(55,52,71, 1)',
            backgroundGradientTo: 'rgba(55,52,71, 1)',
            color: (opacity = 1) => `rgba(206, 201, 233, ${opacity})`
        }
        const data = {
            labels: this.getWeeksString(5),
            datasets: [
                {
                    data: [95, 55, 22, 15, 23],
                    color: (opacity) => `rgba(206, 201, 233, ${1})`,
                },
                {
                    data: [80, 82, 70, 60, 50],
                    color: (opacity) => `rgba(0,0,0, ${1})`,
                },
            ]
        }
        const screenWidth = Dimensions.get('screen').width

        return (
            <ImageBackground source={require('../assets/background.png')} style={{ width: '100%', height: '100%' }}>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.calorieGraphVisible}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                        {!this.props.calories.length !== 0 && <View style={{ alignItems: 'center' }}><Text style={{ ...robotoWeights.light, color: 'rgba(46, 139, 87, 1)', fontSize: 18 }}>
                            RECOMMENDED CALORIE
                        </Text>
                            <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233, 1)', fontSize: 18 }}>
                                YOUR CALORIE
                        </Text>
                            <LineChart
                                data={this.getCaloriGraphData()}
                                width={screenWidth}
                                height={220}
                                chartConfig={chartConfig}
                            />
                        </View>
                        }
                        {
                            this.props.calories.length === 0 &&
                            <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233, 1)', fontSize: 18 }}>
                                NO GRAPH TO BE SHOWN
                            </Text>
                        }

                        <TouchableOpacity onPress={() => this.setCalorieGraphVisible(false)} style={{ margin: 5, height: 40, width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                            <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)' }}>BACK</Text>
                        </TouchableOpacity>

                    </View>
                </Modal>
                <ScrollView>
                    <View style={{ alignContent: 'center', alignItems: 'center', marginTop: 22 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Avatar onPress={() => Actions.register({ editUser: true })} size={'xlarge'} rounded source={{ uri: this.props.image }}
                                icon={{ name: 'user', type: 'font-awesome' }} />
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 18 }}>{this.props.name}</Text>
                        </View>
                        <View style={{ marginTop: 30 }}>
                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => {
                                    if (!this.props.user.bmi.calorie) {
                                        Alert.alert('Failed', 'Calculate your BMI before proceeding with this.')
                                    } else {
                                        if (this.props.calories.length !== 0) {
                                            this.setCalorieGraphVisible(true)
                                        } else {
                                            Alert.alert('Failed', 'There are no calories yet!')
                                        }
                                    }
                                }}>
                                    <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 18, textDecorationLine: 'underline' }}>
                                        Show Calorie Graph
                                </Text>
                                </TouchableOpacity>
                                <Text style={{ ...robotoWeights.light, color: 'rgb(0,0,0)', fontSize: 18 }}>
                                    WEIGHT
                                </Text>
                                <Text style={{ ...robotoWeights.light, color: 'rgb(206, 201, 233)', fontSize: 18 }}>
                                    BMI
                                </Text>
                            </View>
                            {!this.props.loading && <LineChart
                                data={this.getGraphData()}
                                width={screenWidth}
                                height={220}
                                chartConfig={chartConfig}
                            />}
                        </View>
                        <View style={{
                            backgroundColor: '#312e3f',
                            height: 40,
                            width: 250,
                            margin: 10,
                            borderRadius: 5,
                            padding: 3,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                            <Text style={{
                                ...robotoWeights.light,
                                color: 'rgba(206, 201, 233,1)',
                                fontSize: 18,
                                fontWeight: 'bold',
                            }}> {this.props.status} </Text>
                        </View>
                    </View>
                </ScrollView>

            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    image: state.app.user.image,
    name: state.app.user.name,
    status: state.app.user.bmi.name,
    graph: state.app.user.graph,
    bmigraph: state.app.user.bmigraph,
    user: state.app.user,
    dateCreated: state.app.dateCreated,
    loading: state.app.loading,
    date: state.app.date,
    calories: state.app.calories,
    app: state.app
})

const mapDispatchToProps = {
    updateLoading, setDate, resetCalories, resetTotal
}

export default connect(mapStateToProps, mapDispatchToProps)(Statistics)
