import React from 'react'
import { Alert, StyleSheet, ScrollView, View, Image, TouchableOpacity, ImageBackground, Dimensions } from 'react-native'
import { Text, ListItem } from 'react-native-elements'
import Video from 'react-native-video'
import { robotoWeights } from 'react-native-typography'
import Loading from './Loading'
import appFiles from '../assets/appFiles'
import { addToDiary } from '../actions/appActions'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
const { width, height } = Dimensions.get('screen')

class Exercise extends React.Component {

    constructor(props) {
        super(props)
        this.video = Video
        this.state = {
            paused: true,
            value: 0,
            currentTime: 0,
            currentExercise: null,
            exercising: false,
            loading: true,
            exercises: [],
            currentIndex: 0,
            calories: 0,
            time: 0,
            timer: null,
            justClicked: false
        }
    }

    tick = () => this.setState({ time: this.state.time + 1 }, console.log(this.state.time))

    startTimer = () => {
        this.clearTimer() //prevent double call
        this.setState({
            timer: setInterval(this.tick, 1500)
        })
    }

    clearTimer = () => clearInterval(this.state.timer)

    async componentDidMount() {
        Alert.alert('Action Required', 'Please consult to a doctor before taking this exercise.', [
            { text: 'Cancel', onPress: Actions.statistics },
            { text: 'I want to proceed', onPress: () => { } },
            { text: 'Already consulted', onPress: () => { } },
        ])
        new Promise(res => {
            this.setState({ exercises: appFiles.exercises })
            res(true)
        }).then(val => {
            if (this.state.currentExercise === null) {
                this.setState({ currentExercise: this.state.exercises[0] })
            }
        }).catch(e => Alert.alert('Error', `${e.message}`))
    }

    initiateExercise = async () => {
        this.startTimer()
        if (this.state.currentIndex < this.state.exercises.length) {
            this.video.seek(this.state.currentExercise.time, 50)
            this.setState({ exercising: true, paused: false })
        } else {
            await this.setState({ exercising: true, paused: false, currentIndex: 0, currentExercise: this.state.exercises[0] })
            await this.video.seek(this.state.currentExercise.time, 50)
        }
    }

    endExercise = (stop) => {
        this.clearTimer()
        if (stop) {
            this.setState({
                exercising: false,
                paused: true,
            })
        } else {
            if (this.state.currentIndex === this.state.exercises.length - 1) {
                this.setState({
                    paused: true,
                    currentExercise: this.state.exercises[0],
                    exercising: false,
                    exercises: this.state.exercises.map((e, i) => i === 0 ? { ...e, done: false, active: true } : { ...e, done: false, active: false }),
                    currentIndex: 0,
                })
            } else {
                this.setState({
                    currentIndex: this.state.currentIndex += 1,
                    exercising: false, paused: true,
                    calories: this.state.currentExercise.calorie,
                    exercises: this.state.exercises.map(e => e.time === this.state.currentExercise.time ? { ...e, done: true, active: false } : e),
                    currentExercise: this.state.exercises[this.state.currentIndex],
                }, () => {
                    this.setState({ exercises: this.state.exercises.map((e, i) => i === this.state.currentIndex ? { ...e, active: true } : e) })
                })
            }
        }
    }

    stopExercise = () => {
        this.props.addToDiary({
            date: new Date(),
            message: `You have burned ${this.state.calories} cal and spent ${this.getTimeFormat().minutesFormat} minutes and ${this.getTimeFormat().secondsFormat} seconds`
        })
        this.clearExercise()
        Actions.statistics()
    }

    clearExercise = () => {
        this.setState({
            time: 0,
            calories: 0,
            paused: true,
            currentExercise: this.state.exercises[0],
            exercising: false,
            exercises: this.state.exercises.map((e, i) => i === 0 ? { ...e, done: false, active: true } : { ...e, done: false, active: false }),
            currentIndex: 0,
        })
    }

    getTimeFormat = () => {
        let minutes = Math.floor(this.state.time / 60)
        let seconds = this.state.time - minutes * 60
        let minutesFormat = minutes < 10 ? `0${minutes}` : minutes
        let secondsFormat = seconds < 10 ? `0${seconds}` : seconds
        return { format: `${minutesFormat}:${secondsFormat}`, minutesFormat: minutes, secondsFormat: seconds }
    }

    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ height: '100%', width: '100%' }}>
                {this.state.exercising &&
                    <TouchableOpacity onPress={() => this.endExercise(true)} style={{ margin: 5, height: 60, width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)', position: 'absolute', zIndex: 100 }}>
                        <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>BACK</Text>
                    </TouchableOpacity>}
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <TouchableOpacity onPress={() => {
                        if (this.state.paused) {
                            this.startTimer()
                            this.setState({ paused: false })
                        } else {
                            this.clearTimer()
                            this.setState({ paused: true })
                        }
                    }}>
                        <Video
                            onProgress={progress => {
                                if (progress.currentTime >= this.state.currentExercise.end) {
                                    this.endExercise()
                                }
                            }}
                            ref={ref => this.video = ref}
                            paused={this.state.paused}
                            onLoad={() => {
                                this.setState({ loading: false })
                            }}
                            onError={e => Alert.alert('Error', e.error.errorString)}
                            style={[
                                (!this.state.loading && !this.state.exercising) && { display: 'none' },
                                { height: 400, width: width }
                            ]}
                            source={require('../assets//videos/vid.mp4')} />
                    </TouchableOpacity>
                    {this.state.exercising &&
                        <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 30 }}>
                            {this.getTimeFormat().format}
                        </Text>
                    }
                </View>
                {
                    !this.state.exercising && !this.state.loading && <View>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>Burned Calories: {this.state.calories}</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={this.initiateExercise} style={{ margin: 5, height: 'auto', width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                                    <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>START/CONTINUE EXERCISE</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.stopExercise} style={{ margin: 5, height: 'auto', width: 150, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                                    <Text style={{ ...robotoWeights.light, textAlign: 'center', color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>STOP EXERCISE</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <ScrollView>
                            {
                                this.state.exercises.map((e, i) => <ListItem
                                    key={i + 'e'}
                                    title={e.name.toUpperCase()}
                                    containerStyle={{ backgroundColor: e.active ? 'rgba(234,43,122, 1)' : 'rgba(206, 201, 233,1)', margin: 10, borderRadius: 5 }}
                                    rightIcon={{ name: e.done ? 'check' : 'clear' }}
                                    titleStyle={{ ...robotoWeights.light, fontSize: 15 }}
                                />)
                            }
                        </ScrollView>
                    </View>
                }
                {this.state.loading && <Loading />}
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    date: state.app.date
})

const mapDispatchToProps = {
    addToDiary
}

export default connect(mapStateToProps, mapDispatchToProps)(Exercise)
