import React from 'react'
import { Dimensions, Modal, TouchableOpacity, View, TextInput, Alert, StyleSheet, ScrollView, Image, KeyboardAvoidingView, ImageBackground } from 'react-native'
import { CheckBox, Text, Icon, ListItem } from 'react-native-elements'
import { Actions, ActionConst } from 'react-native-router-flux'
import { removeError, selectGender, pushError, updateAge, updateHeight, updateImage, updateName, updateWeight, setUser } from '../actions/registerActions'
import { register, setDate, setDateCreated, addToDiary, updateBMI, addToGraph, updateLoading, addBMIToGraph, resetTotal, resetCalories } from '../actions/appActions'
import { connect } from 'react-redux';
const { width, height } = Dimensions.get('screen')
import { robotoWeights } from 'react-native-typography'
import ImagePicker from 'react-native-image-picker'
import * as method from '../functions'
import DatePicker from 'react-native-datepicker'

class Register extends React.Component {

    state = {
        activityLevel: [
            {
                name: 'Sedentary',
                value: 30,
                checked: true
            },
            {
                name: 'Moderate',
                value: 40,
                checked: false
            },
            {
                name: 'Very Active',
                value: 45,
                checked: false
            },
        ],
        modalVisible: false
    }

    componentDidMount() {
        if (this.props.editUser) {
            this.props.setUser(this.props.user)
        }
    }

    updateCheckBox = index => {
        this.setState({ activityLevel: this.state.activityLevel.map((a, i) => i === index ? { ...a, checked: true } : { ...a, checked: false }) })
    }

    showImagePicker = () => {
        const options = {
            title: 'Select Photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            mediaType: 'photo'
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                Alert.alert('ImagePicker Error', response.error)
                console.log();
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                this.props.updateImage(response.uri)
            }
        });
    }

    setModalVisible = value => this.setState({ modalVisible: value })

    getNewUserInfoArrayForm = () => {
        return Object.keys(this.props.newUser).map(key => {
            let obj = {}
            obj.key = key
            obj.value = this.props.newUser[key]
            return obj
        })
    }

    getBMI = async () => {
        return method.getBMI(parseFloat(this.props.weight), parseFloat(this.props.height), this.state.activityLevel.filter(a => a.checked)[0].value)
    }

    checkFields = () => {
        new Promise((resolve, reject) => {
            let errors = []
            if (this.props.name === '') {
                errors.push('Invalid Name')
            }
            resolve(errors)
        }).then(errors => {
            let editedErrors = errors
            if (this.props.age === '' || isNaN(this.props.age) || parseInt(this.props.age) <= 0 || parseInt(this.props.age) > 150) {
                editedErrors.push('Invalid Age')
            }
            return editedErrors
        }).then(errors => {
            let editedErrors = errors
            if (this.props.weight === '' || isNaN(this.props.weight)) {
                editedErrors.push('Invalid Weight')
            }
            return editedErrors
        }).then(errors => {
            let editedErrors = errors
            if (this.props.height === '' || isNaN(this.props.height)) {
                editedErrors.push('Invalid Height')
            }
            return editedErrors
        }).then(errors => {
            if (errors.length === 0) {
                this.getBMI()
                    .then(bmi => {
                        this.props.setDateCreated(new Date())
                        return bmi
                    })
                    .then(async bmi => {
                        this.props.updateLoading(true)
                        this.props.register(this.props.newUser)
                        if (!this.props.editUser) {
                            this.props.updateBMI(bmi)
                            this.props.addToGraph({
                                label: '0 week',
                                data: parseFloat(this.props.weight)
                            })
                            this.props.addBMIToGraph({
                                label: '0 week',
                                data: parseFloat(bmi.value)
                            })
                        }
                        this.props.updateLoading(false)
                        if (!this.props.loading) {
                            Actions.scenes({ type: ActionConst.RESET })
                        }
                    })
            } else {
                Alert.alert('Cannot proceed! Please check the following errors', `${errors.toString()}`)
            }
        })
    }

    render() {
        return (
            <ImageBackground style={{ height: '100%', width: '100%' }} source={require('../assets/background.png')}>
                <ScrollView>
                    <KeyboardAvoidingView behavior="padding">
                        <View style={{ marginTop: 23 }}>
                            <TouchableOpacity onPress={this.showImagePicker} style={{ alignItems: 'center' }}>
                                <Image
                                    style={{
                                        height: 100,
                                        width: 100,
                                    }}
                                    source={this.props.image ? { uri: this.props.image } : require('../assets/userimage.png')}
                                />
                            </TouchableOpacity>
                            <View style={styles.container}>
                                <TextInput
                                    inlineImageLeft='n'
                                    placeholderTextColor="rgba(206, 201, 233,1)"
                                    style={styles.textInput}
                                    placeholder={'Name'}
                                    returnKeyType='next'
                                    onChangeText={this.props.updateName}
                                    value={this.props.name}
                                    underlineColorAndroid={'transparent'} />

                                <View style={{ alignItems: 'center' }}>
                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                        <CheckBox
                                            textStyle={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', }}
                                            containerStyle={{ backgroundColor: 'rgba(0,0,0, 0.5)', borderRadius: 5, width: 150 }}
                                            center
                                            checkedColor={'rgba(206, 201, 233,1)'}
                                            title='Male'
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            onPress={() => {
                                                this.props.selectGender('male')
                                            }}
                                            checked={this.props.maleChecked}
                                        />
                                        <CheckBox
                                            textStyle={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', }}
                                            containerStyle={{ backgroundColor: 'rgba(0,0,0, 0.5)', borderRadius: 5, width: 150 }}
                                            center
                                            checkedColor={'rgba(206, 201, 233,1)'}
                                            title='Female'
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            onPress={() => {
                                                this.props.selectGender('female')
                                            }}
                                            checked={this.props.femaleChecked}
                                        />
                                    </View>
                                </View>

                                <TextInput
                                    inlineImageLeft='a'
                                    placeholderTextColor="rgba(206, 201, 233,1)"
                                    style={styles.textInput}
                                    placeholder={'Age'}
                                    returnKeyType='next'
                                    onChangeText={this.props.updateAge}
                                    value={this.props.age}
                                    underlineColorAndroid={'transparent'} />
                                <TextInput
                                    inlineImageLeft='w'
                                    editable={!this.props.editUser}
                                    placeholderTextColor="rgba(206, 201, 233,1)"
                                    style={styles.textInput}
                                    placeholder={'Weight (kg)'}
                                    returnKeyType='next'
                                    onChangeText={this.props.updateWeight}
                                    value={this.props.weight}
                                    underlineColorAndroid={'transparent'} />

                                <TextInput
                                    inlineImageLeft='h'
                                    editable={!this.props.editUser}
                                    placeholderTextColor="rgba(206, 201, 233,1)"
                                    style={styles.textInput}
                                    placeholder={'Height (cm)'}
                                    returnKeyType='next'
                                    onChangeText={this.props.updateHeight}
                                    value={this.props.height}
                                    underlineColorAndroid={'transparent'} />

                                <View style={{ alignItems: 'center' }}>
                                    <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>ACTIVITY LEVEL</Text>
                                    {
                                        this.state.activityLevel.map((a, i) =>
                                            <CheckBox
                                                key={'c' + i}
                                                center
                                                checked={a.checked}
                                                title={a.name}
                                                textStyle={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', }}
                                                containerStyle={{ backgroundColor: 'rgba(0,0,0, 0.5)', borderRadius: 5, borderColor: 'rgba(206, 201, 233,1)', width: 200 }}
                                                checkedColor={'rgba(206, 201, 233,1)'}
                                                uncheckedColor={'rgba(206, 201, 233,1)'}
                                                checkedIcon='dot-circle-o'
                                                uncheckedIcon='circle-o'
                                                onPress={() => this.updateCheckBox(i)}
                                            />)
                                    }
                                </View>

                                <View style={styles.buttonContainer}>
                                    <TouchableOpacity style={styles.button} onPress={this.checkFields}>
                                        <Text style={styles.buttonTitle}> {this.props.editUser ? 'Submit' : 'Register'} </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{ height: 100 }} />
                    </KeyboardAvoidingView>
                </ScrollView>
            </ImageBackground>
        )
    }
}


const styles = StyleSheet.create({
    buttonContainer: {
        paddingTop: 50
    },
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
        marginTop: 50,
    },
    textInput: {
        ...robotoWeights.light,
        textAlign: 'center',
        borderBottomColor: 'rgba(206, 201, 233,0.5)',
        borderBottomWidth: 1,
        color: 'rgba(206, 201, 233,1)',
        height: 50,
        margin: 10,
        borderRadius: 5,
        padding: 3,
    },
    heightInput: {
        backgroundColor: '#ffffff',
        width: 40,
        margin: 10,
        borderRadius: 5,
        padding: 3,
    },
    button: {
        backgroundColor: 'rgba(0,0,0, 0.8)',
        height: 40,
        margin: 10,
        borderRadius: 5,
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTitle: {
        ...robotoWeights.light,
        color: 'rgba(206, 201, 233,1)',
        fontSize: 18,
        fontWeight: 'bold',
    },
    logo: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '30%',
        margin: 10,
    },
    title: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Roboto',
        fontSize: 25,
        margin: 10,
    },
    pageText: {
        color: '#3B5323',
        fontSize: 50
    }
})


const mapStateToProps = state => ({
    maleChecked: state.register.maleChecked,
    femaleChecked: state.register.femaleChecked,
    name: state.register.user.name,
    gender: state.register.user.gender,
    age: state.register.user.age,
    weight: state.register.user.weight,
    height: state.register.user.height,
    image: state.register.user.image,
    errors: state.register.errors,
    newUser: state.register.user,
    user: state.app.user,
    app: state.app,
    loading: state.app.loading,
    date: state.app.date
})

const mapDispatchToProps = {
    removeError, selectGender, pushError, updateAge, updateHeight, updateImage, updateName, updateWeight, register, setDateCreated, setUser, addToDiary, updateBMI, addToGraph, updateLoading, addBMIToGraph, setDate, resetTotal, resetCalories
}
export default connect(mapStateToProps, mapDispatchToProps)(Register)