import React from 'react'
import { Alert, View, ScrollView, ImageBackground, Modal, TouchableOpacity } from 'react-native'
import { ListItem, Text, Icon } from 'react-native-elements'
import { connect } from 'react-redux'
import { robotoWeights } from 'react-native-typography'
import { removeDiaryContent } from '../actions/appActions'

class WeightDiary extends React.Component {

    handlePress = index => {
        Alert.alert('Confirm', 'Are you sure you want to delete this content?', [
            {
                text: 'Yes', onPress: () => this.props.removeDiaryContent(index)
            },
            {
                text: 'No', onPress: () => { }
            }
        ])
    }

    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ height: '100%', width: '100%' }}>
                <ScrollView >
                    <View >
                        <Text style={{ ...robotoWeights.light, fontSize: 15, margin: 5, color: 'rgba(206, 201, 233,1)' }}>
                            Account Created: {`${new Date(this.props.dateCreated).toDateString()}`}
                        </Text>
                    </View>
                    <View style={{ marginTop: 23, alignItems: 'center' }}>
                        <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 25 }}>Weight Diary</Text>
                        {
                            this.props.user.diary.map((d, index) => <ListItem
                                key={index + 'wd'}
                                containerStyle={{ borderColor: 'rgba(206, 201, 233,1)', borderWidth: 1, backgroundColor: 'transparent', borderRadius: 5 }}
                                titleStyle={{ color: 'rgba(206, 201, 233,1)' }}
                                subtitleStyle={{ color: 'rgba(206, 201, 233,1)', textAlign: 'left' }}
                                style={{ width: 300, margin: 10, paddingTop: 10 }}
                                title={`${new Date(d.date).toDateString()} ${new Date(d.date).toLocaleTimeString()} `}
                                onPress={() => this.handlePress(index)}
                                subtitle={d.message}
                            />)
                        }
                        {this.props.user.diary.length === 0 && <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 20, marginTop: 20 }}>There is no diary</Text>}
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    user: state.app.user,
    dateCreated: state.app.dateCreated
})

const mapDispatchToProps = {
    removeDiaryContent
}

export default connect(mapStateToProps, mapDispatchToProps)(WeightDiary)
