import React from 'react'
import { View, ImageBackground, ScrollView, Alert } from 'react-native'
import { Text, ListItem } from 'react-native-elements'
import { connect } from 'react-redux'
import { robotoWeights } from 'react-native-typography'
import {removeIntake} from '../actions/appActions'

class FoodCalorieIntake extends React.Component {
    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ height: '100%', width: '100%' }}>
                <ScrollView >
                    <View style={{ marginTop: 23, alignItems: 'center' }}>
                        <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 25 }}>Food Inake</Text>
                        {
                            this.props.user.intakes.map((d, index) => <ListItem
                                key={index + 'wd'}
                                containerStyle={{ borderColor: 'rgba(206, 201, 233,1)', borderWidth: 1, backgroundColor: 'transparent', borderRadius: 5 }}
                                titleStyle={{ color: 'rgba(206, 201, 233,1)' }}
                                subtitleStyle={{ color: 'rgba(206, 201, 233,1)', textAlign: 'left' }}
                                style={{ width: 300, margin: 10, paddingTop: 10 }}
                                title={`${new Date(d.date).toDateString()} ${new Date(d.date).toLocaleTimeString()} `}
                                onPress={() => Alert.alert('Confirm', 'remove this food intake?', [
                                    {
                                        text: 'No', onPress: () => {}
                                    },
                                    {
                                        text: 'Yes', onPress: () => this.props.removeIntake(index)
                                    }
                                ])}
                                subtitle={d.message}
                            />)
                        }
                        {this.props.user.intakes.length === 0 && <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 20, marginTop: 20 }}>There is no food intake</Text>}
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    user: state.app.user
})

const mapDispatchToProps = {
    removeIntake
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodCalorieIntake)