import React from 'react'
import { Alert, View, ScrollView, TextInput, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native'
import { Input, Text, CheckBox } from 'react-native-elements'
import { robotoWeights } from 'react-native-typography'
import { connect } from 'react-redux'
import { addToDiary, updateHeightAndWeight, updateBMI, addToGraph, addBMIToGraph, updateLastBMIUpdate } from '../actions/appActions'
import * as method from '../functions'
import { Actions } from 'react-native-router-flux'
class BMI extends React.Component {

    state = {
        weight: '',
        height: '',
        age: '',
        result: {
            name: '',
            value: 0,
            message: ''
        },
        activityLevel: [
            {
                name: 'Sedentary',
                value: 30,
                checked: true
            },
            {
                name: 'Moderate',
                value: 40,
                checked: false
            },
            {
                name: 'Very Active',
                value: 45,
                checked: false
            },
        ],
        calculatePressed: false
    }

    handleWeightChange = text => {
        this.setState({ weight: text })
    }

    handleHeightChange = text => {
        this.setState({ height: text })
    }

    handleAgeChange = text => {
        this.setState({ age: text })
    }

    updateResult = () => {
        let result = method.getBMI(parseFloat(this.state.weight), parseFloat(this.state.height), this.state.activityLevel.filter(a => a.checked)[0].value, this.props.calories[this.props.calories.length - 1])
        this.setState({ result: result, calculatePressed: true })
    }

    updateCheckBox = index => {
        this.setState({ activityLevel: this.state.activityLevel.map((a, i) => i === index ? { ...a, checked: true } : { ...a, checked: false }) })
    }

    getDayOfTheWeek = day => {
        return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][day]
    }

    saveBMI = () => {
        if (this.state.calculatePressed) {
            Alert.alert('Confirm BMI', 'This may override the existing data in your account. Are you sure to you want save this?', [
                {
                    text: 'Yes', onPress: () => {
                        let diary = {
                            date: new Date(),
                            message: this.state.result.message
                        }
                        this.props.updateHeightAndWeight(this.state.height, this.state.weight)
                        this.props.updateBMI(this.state.result)
                        this.props.addToDiary(diary)
                        if (new Date(this.props.dateCreated).getDay() === new Date().getDay() &&
                            new Date(this.props.dateCreated).toDateString() !== new Date().toDateString() &&
                            new Date().toDateString() !== new Date(this.props.lastBMIUpdate).toDateString()) {
                            this.props.addToGraph({ label: `${this.getWeeksBetween(new Date(this.props.dateCreated), new Date())} week`, data: parseFloat(this.state.weight) })
                            this.props.addBMIToGraph({ label: `${this.getWeeksBetween(new Date(this.props.dateCreated), new Date())} week`, data: parseFloat(this.state.result.value) })
                            this.props.updateLastBMIUpdate(new Date())
                        }
                        this.clearBMI()
                        Actions.statistics()
                    }
                },
                { text: 'No', onPress: () => { } }
            ])
        } else {
            Alert.alert('Failed', 'Press calculate first!')
        }
    }

    componentDidMount() {
        if (this.props.calories.length === 0) {
            Alert.alert('Attention!', 'Set your calorie intake first in Food tab before using this feature.')
        }
    }
    getWeeksBetween = (date1, date2) => {
        var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();
        var difference_ms = Math.abs(date1_ms - date2_ms);
        return Math.floor(difference_ms / ONE_WEEK);
    }

    clearBMI = () => {
        this.setState({
            weight: '',
            height: '',
            age: '',
            result: {
                name: '',
                value: 0,
                message: ''
            },
            calculatePressed: false
        })
    }

    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ height: '100%', width: '100%' }}>
                <ScrollView>
                    <View style={{ alignContent: 'center', alignItems: 'center', marginTop: 23 }}>
                        <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>Calorie Result: {this.props.calories[this.props.calories.length - 1]}</Text>
                        <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>BMI Result: {this.state.result.value.toLocaleString()} </Text>
                        <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>{this.state.result.name} </Text>
                        <View style={styles.container}>
                            <TextInput
                                placeholderTextColor='rgba(206, 201, 233,1)'
                                inlineImageLeft='w'
                                style={styles.textInput}
                                placeholder={'Weight (kg)'}
                                returnKeyType='next'
                                onChangeText={this.handleWeightChange}
                                value={this.state.weight}
                                underlineColorAndroid={'transparent'} />
                            <TextInput
                                placeholderTextColor='rgba(206, 201, 233,1)'
                                inlineImageLeft='h'
                                style={styles.textInput}
                                placeholder={'Height (cm)'}
                                returnKeyType='next'
                                onChangeText={this.handleHeightChange}
                                value={this.state.height}
                                underlineColorAndroid={'transparent'} />
                            <TextInput
                                placeholderTextColor='rgba(206, 201, 233,1)'
                                inlineImageLeft='a'
                                style={styles.textInput}
                                placeholder={'Age'}
                                returnKeyType='next'
                                onChangeText={this.handleAgeChange}
                                value={this.state.age}
                                underlineColorAndroid={'transparent'} />
                            <View style={{ alignItems: 'center' }}>
                                <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 20 }}>ACTIVITY LEVEL</Text>
                                {
                                    this.state.activityLevel.map((a, i) =>
                                        <CheckBox
                                            key={'c' + i}
                                            center
                                            checked={a.checked}
                                            title={a.name}
                                            textStyle={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', }}
                                            containerStyle={{ backgroundColor: 'rgba(0,0,0, 0.5)', borderRadius: 5, borderColor: 'rgba(206, 201, 233,1)', width: 200 }}
                                            checkedColor={'rgba(206, 201, 233,1)'}
                                            uncheckedColor={'rgba(206, 201, 233,1)'}
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            onPress={() => this.updateCheckBox(i)}
                                        />)
                                }
                            </View>
                            <TouchableOpacity onPress={this.updateResult} style={styles.button}>
                                <Text style={styles.buttonTitle}>CALCULATE</Text>
                            </TouchableOpacity>
                            <View style={{ backgroundColor: 'transparent', height: 'auto', width: 300, marginTop: 50, borderRadius: 5, borderWidth: 1, borderColor: 'rgba(206, 201, 233,1)', }}>
                                <Text style={{ ...robotoWeights.light, color: 'rgba(206, 201, 233,1)', fontSize: 15, margin: 10, textAlign: 'center' }}>
                                    Message/Recommendation {'\n'}
                                    {this.state.result.message}
                                </Text>
                            </View>
                            <TouchableOpacity style={styles.button} onPress={this.saveBMI}>
                                <Text style={styles.buttonTitle}>Save</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}



const styles = StyleSheet.create({
    textInput: {
        ...robotoWeights.light,
        backgroundColor: 'transparent',
        borderBottomColor: 'rgba(206, 201, 233,0.5)',
        borderBottomWidth: 1,
        color: 'rgba(206, 201, 233,1)',
        textAlign: 'center',
        height: 50,
        margin: 10,
        borderRadius: 5,
        padding: 3,
    },
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
        marginTop: 25,
    },
    button: {
        backgroundColor: 'rgba(0,0,0, 0.8)',
        height: 40,
        margin: 10,
        borderRadius: 5,
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTitle: {
        ...robotoWeights.light,
        color: 'rgba(206, 201, 233,1)',
        fontSize: 18,
        fontWeight: 'bold',
    }
})

const mapStateToProps = state => ({
    user: state.app.user,
    dateCreated: state.app.dateCreated,
    calories: state.app.calories,
    date: state.app.date,
    lastBMIUpdate: state.app.lastBMIUpdate
})

const mapDispatchToProps = {
    addToDiary, updateHeightAndWeight, updateBMI, addToGraph, addBMIToGraph, updateLastBMIUpdate,
}

export default connect(mapStateToProps, mapDispatchToProps)(BMI)
