import * as types from '../actionTypes/foodActionTypes'

export const changeSearchText = (text, result) => {
    return dispatch => {
        dispatch({ type: types.CHANGE_SEARCH_TEXT, text: text, result: result })
    }
}

export const addFood = food => {
    return dispatch => {
        dispatch({ type: types.ADD_FOOD, food: food })
    }
}

export const removeFood = index => {
    return dispatch => {
        dispatch({ type: types.REMOVE_FOOD, index: index })
    }
}

export const changeFoodCount = (index, count) => {
    return dispatch => {
        dispatch({ type: types.CHANGE_FOOD_COUNT, count: count, index: index })
    }
}

export const calculate = () => {
    return dispatch => {

    }
}