import * as types from '../actionTypes/registerActionTypes'

export const updateName = text => {
    return dispatch => {
        dispatch({ type: types.UPDATE_NAME, text: text })
    }
}

export const selectGender = gender => {
    return dispatch => {
        if (gender === 'male') {
            dispatch({ type: types.CHANGE_GENDER, gender: gender, maleChecked: true, femaleChecked: false })
        } else if (gender === 'female') {
            dispatch({ type: types.CHANGE_GENDER, gender: gender, maleChecked: false, femaleChecked: true })
        }
    }
}

export const setUser = user => {
    return dispatch => {
        dispatch({ type: types.SET_USER, user: user })
    }
}

export const updateAge = age => {
    return dispatch => {
        dispatch({ type: types.UPDATE_AGE, age: age })
    }
}

export const updateHeight = height => {
    return dispatch => {
        dispatch({ type: types.UPDATE_HEIGHT, height: height })
    }
}

export const updateWeight = weight => {
    return dispatch => {
        dispatch({ type: types.UPDATE_WEIGHT, weight: weight })
    }
}

export const pushError = message => {
    return dispatch => {
        dispatch({ type: types.ERROR, error: message })
    }
}

export const removeError = () => {
    return dispatch => {
        dispatch({ type: types.REMOVE_ERROR })
    }
}

export const updateImage = uri => {
    return dispatch => {
        dispatch({ type: types.UPDATE_IMAGE, uri: uri })
    }
}