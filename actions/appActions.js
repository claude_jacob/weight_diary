import * as types from '../actionTypes/appActionTypes'
import * as methods from '../functions/index'

export const register = user => {
    return dispatch => {
        dispatch({ type: types.REGISTER, user: { ...user, bmi: methods.getBMI(parseFloat(user.weight), parseFloat(user.height), null) } })
    }
}

export const addToDiary = content => {
    return dispatch => {
        dispatch({ type: types.ADD_TO_DIARY, content: content })
    }
}

export const updateWeightList = (weight, week) => {
    return dispatch => {
        dispatch({ type: types.UPDATE_WEIGHT_LIST, point: { weight: weight, week: week } })
    }
}

export const updateHeightAndWeight = (height, weight) => {
    return dispatch => {
        dispatch({ type: types.UPDATE_HEIGHT_AND_WEIGHT, height: height, weight: weight })
    }
}

export const removeDiaryContent = index => {
    return dispatch => {
        dispatch({ type: types.REMOVE_DIARY_CONTENT, index: index })
    }
}

export const updateBMI = bmi => {
    return dispatch => {
        dispatch({ type: types.UPDATE_BMI, bmi: bmi })
    }
}

export const setDateCreated = date => {
    return dispatch => {
        dispatch({ type: types.SET_DATE_CREATED, date: date })
    }
}

export const addToGraph = data => {
    return dispatch => {
        dispatch({ type: types.ADD_TO_GRAPH, data: data })
    }
}

export const updateLoading = value => {
    return dispatch => {
        dispatch({ type: types.UPDATE_LOADING, value: value })
    }
}

export const addBMIToGraph = bmi => {
    return dispatch => {
        dispatch({ type: types.ADD_BMI_TO_GRAPH, bmi: bmi })
    }
}

export const setUserCalorie = cal => {
    return dispatch => {
        dispatch({ type: types.SET_USER_CALORIE, calorie: cal })
    }
}

export const addUserCalorie = cal => {
    return dispatch => {
        dispatch({ type: types.ADD_USER_CALORIE, calorie: cal })
    }
}

export const updateLastCalorieDate = date => {
    return dispatch => {
        dispatch({ type: types.LAST_CALORIE_UPDATE, date: date })
    }
}

export const updateTotal = total => {
    return dispatch => {
        dispatch({ type: types.UPDATE_TOTAL, total: total })
    }
}

export const resetCalories = () => {
    return dispatch => {
        dispatch({ type: types.RESET_CALORIES })
    }
}

export const setDate = date => {
    return dispatch => {
        dispatch({ type: types.SET_DATE, date: date })
    }
}

export const resetTotal = () => {
    return dispatch => {
        dispatch({ type: types.RESET_TOTAL })
    }
}

export const updateLastBMIUpdate = date => {
    return dispatch => {
        dispatch({ type: types.LAST_BMI_UPDATE, date: date })
    }
}

export const addToIntake = intake => {
    return dispatch => {
        dispatch({ type: types.ADD_TO_INTAKE, intake: intake })
    }
}

export const removeIntake = index => {
    return dispatch => {
        dispatch({ type: types.REMOVE_INTAKE, index: index })
    }
}