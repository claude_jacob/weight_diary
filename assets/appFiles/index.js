export default {
    exercises: [
        {
            time: 22,
            end: 60,
            name: 'March in place',
            calorie: 6,
            done: false,
            active: true
        },
        {
            time: 60,
            end: 91,
            name: 'Jog in place',
            calorie: 12,
            done: false,
            active: false
        },
        {
            time: 91,
            end: 123,
            name: 'Torso twist',
            calorie: 17,
            done: false,
            active: false
        },
        {
            time: 123,
            end: 155,
            name: 'Row + Lateral Steps',
            calorie: 23,
            done: false,
            active: false
        },
        {
            time: 155,
            end: 203,
            name: 'Squats',
            calorie: 30,
            done: false,
            active: false
        },
        {
            time: 203,
            end: 248,
            name: 'Slow Burpees',
            calorie: 41,
            done: false,
            active: false
        },
        {
            time: 248,
            end: 263,
            name: 'Active Rest',
            calorie: 43,
            done: false,
            active: false
        },
        {
            time: 263,
            end: 301,
            name: 'Jumping Jacks',
            calorie: 51,
            done: false,
            active: false
        },
        {
            time: 301,
            end: 318,
            name: 'Active Rest',
            calorie: 54,
            done: false,
            active: false
        },
        {
            time: 318,
            end: 358,
            name: 'Slow Burpees',
            calorie: 61,
            done: false,
            active: false
        },
        {
            time: 358,
            end: 373,
            name: 'Active Rest',
            calorie: 64,
            done: false,
            active: false
        },
        {
            time: 373,
            end: 412,
            name: 'Jumping Jacks',
            calorie: 73,
            done: false,
            active: false
        },
        {
            time: 420,
            end: 465,
            name: 'Lateral Step + Reach',
            calorie: 81,
            done: false,
            active: false
        },
        {
            time: 465,
            end: 481,
            name: 'Active Rest',
            calorie: 84,
            done: false,
            active: false
        },
        {
            time: 481,
            end: 519,
            name: 'Ventral Jacks',
            calorie: 91,
            done: false,
            active: false
        },
        {
            time: 519,
            end: 536,
            name: 'Active Rest',
            calorie: 94,
            done: false,
            active: false
        },
        {
            time: 536,
            end: 575,
            name: 'Lateral Step + Reach',
            calorie: 102,
            done: false,
            active: false
        },
        {
            time: 575,
            end: 589,
            name: 'Active Rest',
            calorie: 104,
            done: false,
            active: false
        },
        {
            time: 589,
            end: 630,
            name: 'Ventral Jacks',
            calorie: 112,
            done: false,
            active: false
        },
        {
            time: 635,
            end: 687,
            name: 'High Knee Pause',
            calorie: 123,
            done: false,
            active: false
        },
        {
            time: 688,
            end: 702,
            name: 'Active Rest',
            calorie: 125,
            done: false,
            active: false
        },
        {
            time: 702,
            end: 742,
            name: 'Fly Jacks',
            calorie: 133,
            done: false,
            active: false
        },
        {
            time: 742,
            end: 757,
            name: 'Active Rest',
            calorie: 136,
            done: false,
            active: false
        },
        {
            time: 757,
            end: 798,
            name: 'High Knee Pause',
            calorie: 143,
            done: false,
            active: false
        },
        {
            time: 798,
            end: 812,
            name: 'Active Rest',
            calorie: 146,
            done: false,
            active: false
        },
        {
            time: 812,
            end: 852,
            name: 'Fly Jacks',
            calorie: 153,
            done: false,
            active: false
        },
        {
            time: 871,
            end: 920,
            name: 'Bicycle Crunches',
            calorie: 166,
            done: false,
            active: false
        },
        {
            time: 920,
            end: 936,
            name: 'Active Rest',
            calorie: 169,
            done: false,
            active: false
        },
        {
            time: 936,
            end: 975,
            name: 'Lunge Jacks',
            calorie: 176,
            done: false,
            active: false
        },
        {
            time: 975,
            end: 991,
            name: 'Active Rest',
            calorie: 179,
            done: false,
            active: false
        },
        {
            time: 991,
            end: 1030,
            name: 'Bicycle Crunches',
            calorie: 186,
            done: false,
            active: false
        },
        {
            time: 1031,
            end: 1045,
            name: 'Active Rest',
            calorie: 189,
            done: false,
            active: false
        },
        {
            time: 1045,
            end: 1085,
            name: 'Lunge Jacks',
            calorie: 197,
            done: false,
            active: false
        },
        {
            time: 1094,
            end: 1136,
            name: 'Plank Slaps',
            calorie: 206,
            done: false,
            active: false
        },
        {
            time: 1136,
            end: 1151,
            name: 'Active Rest',
            calorie: 209,
            done: false,
            active: false
        },
        {
            time: 1151,
            end: 1191,
            name: 'Stutter Jacks',
            calorie: 216,
            done: false,
            active: false
        },
        {
            time: 1191,
            end: 1207,
            name: 'Active Rest',
            calorie: 219,
            done: false,
            active: false
        },
        {
            time: 1207,
            end: 1245,
            name: 'Plank Slaps',
            calorie: 227,
            done: false,
            active: false
        },
        {
            time: 1245,
            end: 1261,
            name: 'Active Rest',
            calorie: 229,
            done: false,
            active: false
        },
        {
            time: 1261,
            end: 1302,
            name: 'Stutter Jacks',
            calorie: 237,
            done: false,
            active: false
        },
        {
            time: 1306,
            end: 1352,
            name: 'Lateral Hops',
            calorie: 246,
            done: false,
            active: false
        },
        {
            time: 1352,
            end: 1367,
            name: 'Active Rest',
            calorie: 249,
            done: false,
            active: false
        },
        {
            time: 1367,
            end: 1406,
            name: 'High Knee Jacks',
            calorie: 257,
            done: false,
            active: false
        },
        {
            time: 1406,
            end: 1422,
            name: 'Active Rest',
            calorie: 260,
            done: false,
            active: false
        },
        {
            time: 1422,
            end: 1461,
            name: 'Lateral Hops',
            calorie: 267,
            done: false,
            active: false
        },
        {
            time: 1461,
            end: 1476,
            name: 'Active Rest',
            calorie: 270,
            done: false,
            active: false
        },
        {
            time: 1476,
            end: 1516,
            name: 'High Knee Jacks',
            calorie: 277,
            done: false,
            active: false
        },
        {
            time: 1524,
            end: 1570,
            name: 'Leg Raise + Crunch',
            calorie: 287,
            done: false,
            active: false
        },
        {
            time: 1570,
            end: 1585,
            name: 'Active Rest',
            calorie: 290,
            done: false,
            active: false
        },
        {
            time: 1585,
            end: 1624,
            name: 'Plank + Leg Raise',
            calorie: 297,
            done: false,
            active: false
        },
        {
            time: 1624,
            end: 1639,
            name: 'Active Rest',
            calorie: 300,
            done: false,
            active: false
        },
        {
            time: 1639,
            end: 1681,
            name: 'Leg Raise + Crunch',
            calorie: 308,
            done: false,
            active: false
        },
        {
            time: 1681,
            end: 1694,
            name: 'Active Rest',
            calorie: 310,
            done: false,
            active: false
        },
        {
            time: 1694,
            end: 1735,
            name: 'Plank + Leg Raise',
            calorie: 318,
            done: false,
            active: false
        },
        {
            time: 1741,
            end: 1785,
            name: 'Toe Touch Kicks',
            calorie: 327,
            done: false,
            active: false
        },
        {
            time: 1785,
            end: 1800,
            name: 'Active Rest',
            calorie: 330,
            done: false,
            active: false
        },
        {
            time: 1800,
            end: 1840,
            name: 'Jumping Jacks',
            calorie: 337,
            done: false,
            active: false
        },
        {
            time: 1840,
            end: 1855,
            name: 'Active Rest',
            calorie: 340,
            done: false,
            active: false
        },
        {
            time: 1855,
            end: 1896,
            name: 'Toe Touch Kicks',
            calorie: 348,
            done: false,
            active: false
        },
        {
            time: 1896,
            end: 1910,
            name: 'Active Rest',
            calorie: 351,
            done: false,
            active: false
        },
        {
            time: 1910,
            end: 1951,
            name: 'Jumping Jacks',
            calorie: 358,
            done: false,
            active: false
        },
        {
            time: 1955,
            end: 2001,
            name: 'Lunging Hamstring Stretch',
            calorie: 367,
            done: false,
            active: false
        },
        {
            time: 2004,
            end: 2045,
            name: 'Quadriceps Stretch',
            calorie: 376,
            done: false,
            active: false
        },
        {
            time: 2048,
            end: 2093,
            name: 'Wall Calf Stretch',
            calorie: 385,
            done: false,
            active: false
        },
        {
            time: 2093,
            end: 2141,
            name: 'Chest Stretch',
            calorie: 394,
            done: false,
            active: false
        },
        {
            time: 2143,
            end: 2173,
            name: 'Wide Hamstring Stretch',
            calorie: 400,
            done: false,
            active: false
        },
        {
            time: 2175,
            end: 2220,
            name: 'Inside Thigh Stretch',
            calorie: 408,
            done: false,
            active: false
        }
    ],
    foods: {
        '0':
            [{
                name: 'Ampalaya Fruit',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                serving: '2 cups raw or 1 cup cooked',
                foodtype: 'vegetable'
            },
            {
                name: 'Cabbage',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Cabbage',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Cauliflower',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Cauliflower',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Eggplant',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Eggplant',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Kangkong',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Kangkong',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Lettuce',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Lettuce',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Malunggay leaves',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cup cooked'
            },
            {
                name: 'Malunggay leaves',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Marshmallow',
                carbohydrate: 5,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'Sugar',
                serving: '1(2-1/2cm diameter)'
            },
            {
                name: 'Okra',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Okra',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Onion',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Onion',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Papaya green',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Papaya green',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            }],
        '1':
            [{
                name: 'Patola',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Petsay',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Radish (labanos)',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Radish (labanos)',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Saluyot',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Saluyot',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Spinach',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'Vegetable A',
                serving: '1cup raw or ½ cooked'
            },
            {
                name: 'Sigarilas',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Sigarilas',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Sitaw leaves',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Tomato',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '1 cups raw or 1/2 cooked'
            },
            {
                name: 'Tomato',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Upo',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cups raw or 1/2 cooked'
            },
            {
                name: 'Upo',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Baguio beans',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable',
                serving: '1 cup raw or 1/2 cooked'
            },
            {
                name: 'Baguio beans',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '2 cups raw or 1 cup cooked'
            },
            {
                name: 'Carrot',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '1/2 cup raw or 1/2 cup cooked'
            }],
        '2':
            [{
                name: 'Coconut shoot (ubod)',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '1/2 cup raw or 1/2 cup cooked'
            },
            {
                name: 'Patani',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '1/2 cup raw or 1/2 cup cooked'
            },
            {
                name: 'Mungbean pods (toge)',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '1/2 cup raw or 1/2 cup cooked'
            },
            {
                name: 'Singkamas',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '1/2 cup raw or 1/2 cup cooked'
            },
            {
                name: 'Squash',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '1/2 cup raw or 1/2 cup cooked'
            },
            {
                name: 'String beans (sitaw)',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable',
                serving: '1/2 cup raw or 1/2 cup cooked'
            },
            {
                name: 'Apple',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1/2 of 8 cm or 1 (6cm diameter)'
            },
            {
                name: 'Banana (Lakatan)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 (9 x 3cm)'
            },
            {
                name: 'Banana (Latundan)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 (9 x 3cm)'
            },
            {
                name: 'Banana (Saba)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 (10 x 4cm)'
            }],
        '3':
            [{
                name: 'Dalanghita',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '2 (6 diameter each)'
            },
            {
                name: 'Grapes',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '10 (2 cm diameter each) or 4 (3 cm diameter each)'
            },
            {
                name: 'Guava',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '2 (4 cm diameter each)'
            },
            {
                name: 'Guyabano',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 slice (8 x 6 x 2 cm) or 1/2 cup'
            },
            {
                name: 'Jackfruit (ripe)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '3 segments (6 cm diameter each)'
            },
            {
                name: 'Lansones',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '7 (4 x 2 cm each)'
            },
            {
                name: 'Lychee',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '5( 2 cm diameter each)'
            },
            {
                name: 'Mango (green)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 slice (11 x 6 cm)'
            },
            {
                name: 'Mango (medium ripe)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 slice (11 x 6 cm)'
            },
            {
                name: 'Mango (ripe)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 slice (12 x 7 cm) or 1/2 cup cubed'
            }],
        '4':
            [{
                name: 'Mango (indian)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 (6 cm diameter)'
            },
            {
                name: 'Mangosteen',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '3 (6 cm diameter each)'
            },
            {
                name: 'Melon',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 slice (12 x 10 x 3 cm) or 1-1/3 cup'
            },
            {
                name: 'Papaya (ripe)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 slice (10 x 6 x 2 cm) or 3/4 cup'
            },
            {
                name: 'Pineapple',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 slice (10 x 6 x 2 cm) or 1/2 cup'
            },
            {
                name: 'Santol',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 (7 cm diameter)'
            },
            {
                name: 'Stawberry',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1-1/4 cups'
            },
            {
                name: 'Suha',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '3 segments (8 x 4 x3 cm each)'
            },
            {
                name: 'Watermelon',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 slice (12 x 6 x 3 cm) or 1 cup'
            },
            {
                name: 'Tamarind (ripe)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '2 of 6 segments each'
            }],
        '5':
            [{
                name: 'Rambutan',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '8 (3 cm diameter each)'
            },
            {
                name: 'Fruit Cocktail',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '3 tablespoons'
            },
            {
                name: 'Pineapple, crushed',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '3 tablespoons'
            },
            {
                name: 'Pineapple, sliced',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 slice (7 cm diameter)'
            },
            {
                name: 'Dried Dates (pitted)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '3 (3x2 cm each)'
            },
            {
                name: 'Mango chips',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '2 (2x8 cm each)'
            },
            {
                name: 'Prunes (seedless)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '3 pieces'
            },
            {
                name:
                    'Canned Sweetened (apple, mango, pineapple-grapefruit, pineapple-orange)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1/4 cup'
            },
            {
                name: 'Canned Unsweetened (orange, pineapple, prune)',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1/3 cup'
            },
            {
                name: 'Bottled Swettened Orange, Guyabano, mango',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1/3 cup'
            }],
        '6':
            [{
                name: 'Banana Cue',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1/2 of 9-1/2 x 4 cm'
            },
            {
                name: 'Banana Chips',
                carbohydrate: 5,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'Sugar',
                serving: '1(6x3-1/2cm)'
            },
            {
                name: 'bukayo',
                carbohydrate: 5,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'Sugar',
                serving: '1(3-1/2x1cm)'
            },
            {
                name: 'Caramel',
                carbohydrate: 5,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'Sugar',
                serving: '1(2x2cm)'
            },
            {
                name: 'Buko Water',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 cup'
            },
            {
                name: 'Maruya',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1/4 of 10-1/2 x 9-1/2 x 1 cm'
            },
            {
                name: 'Turon',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1/2 of 9-1/2 x 3-1/2 x 1 cm'
            },
            {
                name: 'Shank (bias), lean meat (laman) Beef',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice 30 grams, matchbox size  (5 x 3 1/2 x 1-1/2 cm)'
            },
            {
                name: 'Porterhouse steak (tagiliran, gitna) Lean Meat Beef',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice 30 grams, matchbox size  (5 x 3 1/2 x 1-1/2 cm)'
            },
            {
                name: 'Sirloin steak (tagiliran, hulihan) Lean Meat Beef',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice 30 grams, matchbox size  (5 x 3 1/2 x 1-1/2 cm)'
            },
            {
                name:
                    'Centerloin (tagiliran, unahan)Shank (bias), lean meat (laman)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice 30 grams, matchbox size  (5 x 3 1/2 x 1-1/2 cm)'
            },
            {
                name: 'Shank (bias), round (hita) Lean Meat Caraba Beef',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice 30 grams, matchbox size  (5 x 3 1/2 x 1-1/2 cm)'
            },
            {
                name:
                    'Lean Meat Carabe Beef (laman: bahagya, katamtaman at walangtaba)  ',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice 30 grams, matchbox size  (5 x 3 1/2 x 1-1/2 cm)'
            }],
        '7':
            [{
                name: 'Tenderloin, well-trimmed (lomo) Lean Pork',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice, matchbox size 6-1/2 x 3 x 1-1/2 cm'
            },
            {
                name: 'Chicken Leg (hita)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 small leg 30 grams (13-1/2 cm long x 3 cm diameter)'
            },
            {
                name: 'Chicken meat (laman)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice 30 grams, matchbox size (5 x 3-1/2 x 1-1/2 cm)'
            },
            {
                name: 'Chicken breast meat (pitso)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/4 30 grams  breast - 6 cm long'
            },
            {
                name: 'Blood (dugo) - pork, beef, chicken',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            },
            {
                name: 'Gizzard (balun-balunan) - chicken',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            },
            {
                name: 'Heart (puso) - pork, beef, carabeef',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            },
            {
                name: 'Liver (atay) - pork, beef, carabeef, chicken',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            },
            {
                name: ' Lungs (baga) - pork, beef, carabeef',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            },
            {
                name: 'Small intestine (bitukangmaliit)-  pork, beef, carabeef',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            }],
        '8':
            [{
                name: 'Spleen (lapay) - pork, beef, carabeef',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            },
            {
                name: ' Tripe (goto) - beef',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            },
            {
                name: 'Fish (bakoko, bangus, dalag, labahita, lapu-lapu, etc.)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice (7 x 3 x 2 cm) 35 grams'
            },
            {
                name: 'Fish (Hasa-hasa, dalangangbukid)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 (18 x 4-1/2 cm) 35 grams'
            },
            {
                name: 'Fish (Galunggong)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 (14 x 3-1/2 cm 35 grams'
            },
            {
                name: 'Fish (Hito)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/2 of 22 x 5 cm) 35 grams'
            },
            {
                name: 'Fish (Sapsap)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '2 (10 x 5 cm each) 35 grams'
            },
            {
                name: 'Fish (Tilapya)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '2 (12 x 5 cm each) 35 grams'
            },
            {
                name: 'Fish (Tamban)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '2 (12-1/2 x 3 cm each) 35 grams'
            },
            {
                name: 'Fish (Dilis)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/4 cup 35 grams'
            }],
        '9':
            [{
                name: 'Fish (Alamang, tagunton)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1-1/4 tablespoons 30 grams'
            },
            {
                name: 'Aligue (Alimango)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 tablespoons 15 grams'
            },
            {
                name: 'Aligue (Alimasag)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3 tablespoons 50 grams'
            },
            {
                name: 'Alimango / Alimasag, laman',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/4 cup or 1/2 piece medium 20 grams'
            },
            {
                name: 'Lobster',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '2 tablespoons 30 grams'
            },
            {
                name: 'Talangka',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '75 pcs as purchades 30 grams'
            },
            {
                name: 'Shrimp (Sugpo)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '2 (13 cm each) 25 grams'
            },
            {
                name: 'Shrimp (Suwahe)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '5 (13 cm each) 25 grams'
            },
            {
                name: 'Squid (pusit)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3 (7 x 3 cm each)'
            },
            {
                name: 'Shell (Kuhol)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/2 cup shelled or 3 cups w/ shell 50 grams'
            }],
        '10':
            [{
                name: 'Shell (Susongpilipit)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/3 cup shelled or 2 cups w/ shell 65 grams'
            },
            {
                name: 'Dried Daing (Lapu-lapu)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/4 of 30 x 40 cm 20 grams'
            },
            {
                name: 'Dried Daing (Sapsap)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3 (9 x 5 cm each) 20 grams'
            },
            {
                name: 'Dried Daing (Tamban)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 (16 x 3 cm) 20 grams'
            },
            {
                name: 'Dried tinapa (Bangus)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/4 of 20 x 8 cm 30 grams'
            },
            {
                name: 'Dried tinapa (Galunggong)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 (16 x 4 cm) 30 grams'
            },
            {
                name: 'Dried tinapa (Tamban)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 (16 x 5 cm) 25 grams'
            },
            {
                name: 'Dried tuyo (Alamang)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/3 cup 15 grams'
            },
            {
                name: 'Dried tuyo (Ayungin, dilis, sapsap)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '3(11-1/2 x 8 cm each) 20 grams'
            },
            {
                name: 'Driend tuyo (pusit)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 (8 x 1 cm) 15 grams'
            }],
        '11':
            [{
                name: 'Dried canned salmon',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/3 cup flaked 40 grams'
            },
            {
                name: 'Dried canned tuna in brine',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1/3 cup flaked 30 grams'
            },
            {
                name: 'Tocino (lean)',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 172,
                foodtype: 'meat',
                serving: '1 slice (11 x 4 x 0.5 cm) 45 grams'
            },
            {
                name: 'Pechay',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'Vegetable A',
                serving: '1cup raw or ½ cooked'
            },
            {
                name: 'Beef brisket (punta y pecho)',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1 slice, matchbox size  (5 x 3 -1/2 x 1-1/2 cm) 30 grams'
            },
            {
                name: 'Beef plate (tadyang)',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1 slice, matchbox size  (5 x 3 -1/2 x 1-1/2 cm) 30 grams'
            },
            {
                name: 'Beef chuck (paypay)',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1 slice, matchbox size  (5 x 3 -1/2 x 1-1/2 cm) 30 grams'
            },
            {
                name: 'Pork leg (pata)',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1 slice (4 cm diameter x 2 cm thick) 30 grams'
            },
            {
                name: 'Brain (utak) - pork, beef, carabeef',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            },
            {
                name: 'Fish (Karpa)',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1 slice (15 x 7 x 2 cm) 35 grams'
            },
            {
                name: 'Chicken Egg',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1pc 60 grams'
            }],
        '12':
            [{
                name: 'Quail Egg',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '9 pcs. 70 grams'
            },
            {
                name: 'Salted duck\'s egg',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1pc 60 grams'
            },
            {
                name: 'Cheese, cheddar',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'dairy',
                serving: '1 slice (6 x 3 x 2 cm) 35 grams'
            },
            {
                name: 'Chicken wings',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'dairy',
                serving: '1 medium or 2 small 25 grams'
            },
            {
                name: 'Chicken head',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'dairy',
                serving: '2 heads 35 grams'
            },
            {
                name: 'Sardines canned in oil / tomato sauce',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1 (10 x 4-1/2 cm)'
            },
            {
                name: 'Tuna sardines',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1-1/2 of 6 x 4 x 3 cm each'
            },
            {
                name: 'Tuna spread, canned',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1 tablespoon'
            },
            {
                name: 'Corned Beef',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '3 tablespoons 40 grams'
            },
            {
                name: 'Ham sausage',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '3 of 9 cm diameter x 0.3  cm thick each 55 grams'
            }],
        '13':
            [{
                name: 'Soybean cheese, soft (tofu)',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1/2 cup 100 grams'
            },
            {
                name: 'Soybean cheese, soft (tokwa)',
                carbohydrate: 0,
                protein: 8,
                fat: 6,
                kilocalories: 86,
                kilojoules: 360,
                foodtype: 'meat',
                serving: '1 (6 x 6 x 2 cm) 60 grams'
            },
            {
                name: 'Ham (pigue)',
                carbohydrate: 0,
                protein: 8,
                fat: 10,
                kilocalories: 122,
                kilojoules: 510,
                foodtype: 'meat',
                serving: '1 slice (3 cm cube) 35 grams'
            },
            {
                name: 'Pork/Beef Tongue (dila)',
                carbohydrate: 0,
                protein: 8,
                fat: 10,
                kilocalories: 122,
                kilojoules: 510,
                foodtype: 'meat',
                serving: '3/4 cup 35 grams'
            },
            {
                name: 'Duck\'s egg',
                carbohydrate: 0,
                protein: 8,
                fat: 10,
                kilocalories: 122,
                kilojoules: 510,
                foodtype: 'meat',
                serving: '1 pc 70 grams'
            },
            {
                name: 'Egg (Balut)',
                carbohydrate: 0,
                protein: 8,
                fat: 10,
                kilocalories: 122,
                kilojoules: 510,
                foodtype: 'meat',
                serving: '1 pc 65 grams'
            },
            {
                name: 'Egg (Penoy)',
                carbohydrate: 0,
                protein: 8,
                fat: 10,
                kilocalories: 122,
                kilojoules: 510,
                foodtype: 'meat',
                serving: '1 pc 60 grams'
            },
            {
                name: 'Cheese, filled',
                carbohydrate: 0,
                protein: 8,
                fat: 10,
                kilocalories: 122,
                kilojoules: 510,
                foodtype: 'dairy',
                serving: '1 slice (6 x 3 x 2-1/2 cm) 50 grams'
            },
            {
                name: 'Longanisa, chorizo',
                carbohydrate: 0,
                protein: 8,
                fat: 10,
                kilocalories: 122,
                kilojoules: 510,
                foodtype: 'meat',
                serving: '1 (12 x 2 cm) 25 grams'
            },
            {
                name: 'Milk, evaporated, filled, undiluted',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'dairy',
                serving: '1/2 cup 125 grams'
            }],
        '14':
            [{
                name: 'Milk, evaporated, recombined, undiluted',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'dairy',
                serving: '1/2 cup 125 grams'
            },
            {
                name: 'Milk, fresh carabao\'s',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'dairy',
                serving: '1 cup 250 grams'
            },
            {
                name: 'Milk, fresh cow\'s',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'dairy',
                serving: '1 cup 250 grams'
            },
            {
                name: 'Milk, powdered',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'dairy',
                serving: '1/4 cup 30 grams'
            },
            {
                name: 'Low fat Powdered Milk',
                carbohydrate: 12,
                protein: 8,
                fat: 5,
                kilocalories: 125,
                kilojoules: 523,
                foodtype: 'dairy',
                serving: '1/4 cup 30 grams'
            },
            {
                name: 'Lite low fat milk',
                carbohydrate: 12,
                protein: 8,
                fat: 5,
                kilocalories: 125,
                kilojoules: 523,
                foodtype: 'dairy',
                serving: '1 tetra-brick 250 grams'
            },
            {
                name: 'Skimmed liquid butter milk',
                carbohydrate: 12,
                protein: 8,
                fat: 5,
                kilocalories: 125,
                kilojoules: 523,
                foodtype: 'dairy',
                serving: '2/3 cup 185 grams'
            },
            {
                name: 'Skimmed Powdered milk',
                carbohydrate: 12,
                protein: 8,
                fat: 5,
                kilocalories: 125,
                kilojoules: 523,
                foodtype: 'dairy',
                serving: '1/4 cup 25 grams'
            },
            {
                name: 'Long life skimmed milk',
                carbohydrate: 12,
                protein: 8,
                fat: 5,
                kilocalories: 125,
                kilojoules: 523,
                foodtype: 'dairy',
                serving: '1 cup 250 grams'
            },
            {
                name: 'Yoghurt',
                carbohydrate: 12,
                protein: 8,
                fat: 5,
                kilocalories: 125,
                kilojoules: 523,
                foodtype: 'dairy',
                serving: '1/2 cup 125 grams'
            }],
        '15':
            [{
                name: 'Rice, cooked',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1/2 cup 80 grams'
            },
            {
                name: 'Lugaw',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '3 cups 435 grams'
            },
            {
                name: 'Bibingka',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1 slice (1/4 of 15cm diameter,2cm thick) 40 grams'
            },
            {
                name: 'Biko',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1 slice (10 x 5 x 1 cm) 40 grams'
            },
            {
                name: 'Casava cake',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1/2 of 15 x 3 x 2 cm, 40 grams'
            },
            {
                name: 'Espasol',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '2 (11 x 2-1/2 x 1-1/2 cm each) 35 grams'
            },
            {
                name: 'Kalamay (Latik)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1 (4 x 6 x 2 cm) 50 grams'
            },
            {
                name: 'Kalamay (Ube)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1 slice (7 x 3 x 1-1/2 cm) 55 grams'
            },
            {
                name: 'Kalamay (Kutsinta)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1 (6 cm diameter x 2-1/2 cm) 60 grams'
            },
            {
                name: 'Kalamay (Palitaw)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '4 (7-1/2 x 4 x 0.3 cm each) 55 grams'
            }],
        '16':
            [{
                name: 'Puto: Puti',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1 slice (9-1/2 x 3 x 3-1/2 cm) 45 grams'
            },
            {
                name: 'Sapin-sapin',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1 slice (5 x 3 x 4 cm each) 75 grams'
            },
            {
                name: 'Rolls (hotdog / hamburgers ) z5c',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'bread',
                serving: '1 pc 40 grams'
            },
            {
                name: 'Whole wheat bread',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'bread',
                serving: '2 slice 45 grams'
            },
            {
                name: 'Sponge cake',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'bread',
                serving: ' 1 slice 40 grams'
            },
            {
                name: 'Bread (Pasensiya)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'bread',
                serving: '22 pcs 30 grams'
            },
            {
                name: 'Bread (Lady fingers)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'bread',
                serving: '5 pcs 30 grams'
            },
            {
                name: 'Bread (Mamon tostado)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'bread',
                serving: '3 pcs 30 grams'
            },
            {
                name: 'Bread (Hopia)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'bread',
                serving: '1 1/2 round 35 grams'
            },
            {
                name: 'Bread (Ensaymada)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'bread',
                serving: '1 pc 35 grams'
            }],
        '17':
            [{
                name: 'Corn boiled',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'fruit',
                serving: '1 pc 65 grams'
            },
            {
                name: 'Baby corn',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'fruit',
                serving: '1 cup 90 grams'
            },
            {
                name: 'Bihon, macaroni, sotanghon, spaghetti',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'bread',
                serving: '1 cup 75 grams'
            },
            {
                name: 'Sweet potato',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'vegetable',
                serving: '11 cm 80 grams'
            },
            {
                name: 'Root crop (Cassava)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'vegetable',
                serving: '1 (5 cm long x 4-1/2 cm diameter) or 1/2 cup 85 grams'
            },
            {
                name: 'Root crop (Gabi)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'vegetable',
                serving: '2 (6 cm long x 4 cm diameter each) or 1 cup 100 grams'
            },
            {
                name: 'Root crop (Potato)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'vegetable',
                serving:
                    '2-1/2 of 7 cm long x 4 cm diameter each or 1-1/3 cups 165 grams grams'
            },
            {
                name: 'Root crop (Ubi)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'vegetable',
                serving:
                    '1 (8-1/2 cm long x 4-1/2 cm diameter) or 1-1/3 cups 130 grams'
            },
            {
                name: 'Beans and nuts chesnut, roasted (kastanyas, binusa)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'vegetable',
                serving: '11 piece large or 20 pieces small 40 grams'
            },
            {
                name:
                    'Breakfast cereals, cornstarch, flour (all purpose), Ice cream (regular), Sago (cooked)',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'vegetable',
                serving: '1/2 cup 120 grams'
            }],
        '18':
            [{
                name: 'Beer, cerveza',
                carbohydrate: 3.6,
                protein: 0.5,
                fat: 3,
                kilocalories: 163,
                kilojoules: 550,
                foodtype: 'beverage',
                serving: '1 glass- 6 oz'
            },
            {
                name: 'Cream Cheese',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'Fat',
                serving: '1 Tbspn'
            },
            {
                name: 'Gin, (Ginebra)',
                carbohydrate: 0,
                protein: 0.5,
                fat: 18,
                kilocalories: 832,
                kilojoules: 550,
                foodtype: 'beverage',
                serving: '1 bottle - 12 oz'
            },
            {
                name: 'Brandy',
                carbohydrate: 0,
                protein: 0.5,
                fat: 1,
                kilocalories: 75,
                kilojoules: 550,
                foodtype: 'beverage',
                serving: '1 bottle glass'
            },
            {
                name: 'Rum',
                carbohydrate: 0,
                protein: 0.5,
                fat: 2,
                kilocalories: 107,
                kilojoules: 550,
                foodtype: 'beverage',
                serving: '1 jigger'
            },
            {
                name: 'Tuba',
                carbohydrate: 0,
                protein: 0.5,
                fat: 2,
                kilocalories: 89,
                kilojoules: 550,
                foodtype: 'beverage',
                serving: '1 glass'
            },
            {
                name: 'Whiskey',
                carbohydrate: 0,
                protein: 0.5,
                fat: 2,
                kilocalories: 107,
                kilojoules: 550,
                foodtype: 'beverage',
                serving: '1 jigger'
            },
            {
                name: 'Wine',
                carbohydrate: 0,
                protein: 0.5,
                fat: 2,
                kilocalories: 73,
                kilojoules: 550,
                foodtype: 'beverage',
                serving: '1 glass'
            },
            {
                name: 'Soft drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 100,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 bottle regular size'
            },
            {
                name: 'Grape flavored drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 teaspoon'
            },
            {
                name: 'Grapefruit, lemon, orange, strawberry flavored drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '2 teaspoons'
            }],
        '19':
            [{
                name: 'Flavored fruit drink (Mango, guwayabano, pineapple-pomelo, pomelo)',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '4 teaspoons'
            },
            {
                name: 'Powdered flavored drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 teaspoon'
            },
            {
                name: 'Powdered Apple flavored drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 130,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Powdered Guwayabano flavored drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 150,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Powdered Mango flavored drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 110,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: ''
            },
            {
                name: 'Powdered Melon flavored drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 170,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Powdered Orange flavored drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 140,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Powdered Pineapple flavored drink',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 120,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Plastic bottled juice',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 90,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 small plastic bottle'
            },
            {
                name: 'Chocolate flavored milk drink',
                carbohydrate: 31,
                protein: 2,
                fat: 5,
                kilocalories: 200,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            }],
        '20':
            [{
                name: 'Banana split flavored milk drink',
                carbohydrate: 29,
                protein: 6,
                fat: 2,
                kilocalories: 160,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Melon recomb. flavored milk drink',
                carbohydrate: 31,
                protein: 8,
                fat: 5,
                kilocalories: 200,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Strawberry full cream flavored milk drink',
                carbohydrate: 31,
                protein: 8,
                fat: 8,
                kilocalories: 220,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Mocca flavored milk drink',
                carbohydrate: 28,
                protein: 7,
                fat: 7,
                kilocalories: 210,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Vanilla flavored milk drink',
                carbohydrate: 28,
                protein: 7,
                fat: 7,
                kilocalories: 210,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Chocolate fruit flavored milk drink',
                carbohydrate: 29,
                protein: 8,
                fat: 7,
                kilocalories: 210,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 tetra-brick'
            },
            {
                name: 'Klim lite powdered drink',
                carbohydrate: 12,
                protein: 8,
                fat: 3,
                kilocalories: 103,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '4 tablespoons'
            },
            {
                name: 'Cocoa powdered drink',
                carbohydrate: 12,
                protein: 5,
                fat: 5,
                kilocalories: 68,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '5 tablespoons'
            },
            {
                name: 'Milo powdered drink',
                carbohydrate: 12,
                protein: 2,
                fat: 0.1,
                kilocalories: 57,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '2-1/2 tablespoons'
            },
            {
                name: 'Diet cola',
                carbohydrate: 0.3,
                protein: 0.3,
                fat: 0,
                kilocalories: 3,
                kilojoules: 0,
                foodtype: 'beverage',
                serving: '1 can'
            }],
        '21':
            [{
                name: 'Mungbean Sprout (toge)',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                foodtype: '‘vegetable B',
                kilojoules: 67,
                serving: '/2 cup raw or ½ cup cooked'
            },
            {
                name: 'Banana Heart (puso ng saging)',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable A',
                serving: '1 cup raw or ½ cup cooked'
            },
            {
                name: 'Celery',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                foodtype: 'vegetable A',
                serving: '1 cup raw or ½ cup cooked'
            },
            {
                name: 'Cucumber',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable A',
                serving: '1 cup raw or ½ cup cooked'
            },
            {
                name: 'Gabi leaves',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable A',
                serving: '1 cup raw or ½ cup cooked'
            },
            {
                name: 'Mushroom fresh',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable A',
                serving: '1 cup raw or ½ cup cooked'
            },
            {
                name: 'Pepper fruit',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable A',
                serving: '1 cup raw or ½ cup cooked'
            },
            {
                name: 'Pepper leaves',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 0,
                kilojoules: 0,
                foodtype: 'vegetable A',
                serving: '1 cup raw or ½ cup cooked'
            },
            {
                name: 'Saluyot',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable A',
                serving: '1 cup raw or ½ cup cooked'
            },
            {
                name: 'Langkang hilaw',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable B',
                serving: '1/2 cup raw or ½ cup cooked'
            },
            {
                name: 'Baby corn',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable B',
                serving: '2 (8 cm long x 5-1/2 cm)'
            }],
        '22':
            [{
                name: 'Green peas',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable B',
                serving: '1 tablespoon'
            },
            {
                name: 'Golden Sweet corn, Canned',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable B',
                serving: '2 tablespoon'
            },
            {
                name: 'Mushroom, canned',
                carbohydrate: 3,
                protein: 1,
                fat: 0,
                kilocalories: 16,
                kilojoules: 67,
                foodtype: 'vegetable B',
                serving: '1/3 cup'
            },
            {
                name: 'Atis',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 (5 cm diameter)'
            },
            {
                name: 'Cashew',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1 (7 x 6-1/2 cm)'
            },
            {
                name: 'Duhat',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '20 (2 cm diameter each)'
            },
            {
                name: 'rambutan',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '8 (3cm diameter)'
            },
            {
                name: 'Star Apple',
                carbohydrate: 10,
                protein: 0,
                fat: 0,
                kilocalories: 40,
                kilojoules: 167,
                foodtype: 'fruit',
                serving: '1/2 of 6 cm diameter'
            },
            {
                name: 'Condensed Milk',
                carbohydrate: 5,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'Sugar',
                serving: '2 Tspn'
            },
            {
                name: 'Sugar (White, Brown, Pure cane,syrup)',
                carbohydrate: 5,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'Sugar',
                serving: '1Tspn'
            },
            {
                name: 'Bacon',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '1 strip -10x3cm'
            },
            {
                name: 'Butter',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '1 teaspoon'
            }],
        '23':
            [{
                name: 'Coconut, grated',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '2 tablespoons'
            },
            {
                name: 'Coconut, cream',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '1 tablespoon'
            },
            {
                name: 'Coconut oil',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '1 teaspoon'
            },
            {
                name: 'Latik',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '2 teaspoon/s'
            },
            {
                name: 'Margarine',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '1 teaspoon'
            },
            {
                name: 'Mayonnaise',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '1 teaspoon'
            },
            {
                name: 'Oil (corn, margarine, soybean, rapeseed-canola, rice, sunflower, sesame)',
                carbohydrate: 0,
                protein: 0,
                fat: 4,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '1 teaspoon'
            },
            {
                name: 'Peanut oil, olive oil',
                carbohydrate: 0,
                protein: 0,
                fat: 5,
                kilocalories: 45,
                kilojoules: 0,
                foodtype: 'fat',
                serving: '1 teaspoon'
            },
            {
                name: 'Panutsa',
                carbohydrate: 5,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'Sugar',
                serving: '1Tspn'
            },
            {
                name: 'Pan deleche',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '1 (3x8x8cm)'
            },
            {
                name: 'Pande bonete',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 0,
                foodtype: 'Rice',
                serving: '1(6cm diameter base x 7cm thick)'
            },
            {
                name: 'Pande monay',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 0,
                foodtype: 'Rice',
                serving: '1(10x9x4cm)'
            },
            {
                name: 'pandesal',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '3 (5x5 cm)'
            }],
        '24':
            [{
                name: 'Apas',
                carbohydrate: 23,
                protein: 2,
                fat: 0,
                kilocalories: 100,
                kilojoules: 418,
                foodtype: 'rice',
                serving: '6 (1-1/2 x 12 cm)'
            },
            {
                name: 'French fries',
                carbohydrate: 23,
                protein: 2,
                fat: 5,
                kilocalories: 168,
                kilojoules: 0,
                foodtype: 'rice, fat',
                serving: '1 cup'
            },
            {
                name: 'Meat loaf, canned',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'meat',
                serving: '2 slice (9 x 5 x 1-1/4 cm)'
            },
            {
                name: 'Luncheon Meat',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'meat',
                serving: '2 slice ( 9 x 5 x 1 cm)'
            },
            {
                name: 'Honey',
                carbohydrate: 5,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'Sugar',
                serving: '1Tspn'
            },
            {
                name: 'tahong',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'fish',
                serving: '1/4 cup'
            },
            {
                name: 'Spam',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'meat',
                serving: '3 slice (8 x 5 x 1 cm)'
            },
            {
                name: 'Hotdog',
                carbohydrate: 0,
                protein: 0,
                fat: 0,
                kilocalories: 144,
                kilojoules: 0,
                foodtype: 'Protein',
                serving: '2 (10 x4 cm)'
            },
            {
                name: 'Hamburger',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'meat',
                serving: '2-1/2 (4-1/2 x 1 cm)'
            },
            {
                name: 'Longanisa, Bilbao',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'meat',
                serving: '2 (6 x 2 cm)'
            },
            {
                name: 'Cottage Cheese',
                carbohydrate: 0,
                protein: 8,
                fat: 1,
                kilocalories: 41,
                kilojoules: 0,
                foodtype: 'meat',
                serving: '1/3 cup'
            },
            {
                name: 'Nata de coco',
                carbohydrate: 5,
                protein: 0,
                fat: 0,
                kilocalories: 20,
                kilojoules: 0,
                foodtype: 'Sugar',
                serving: '2Tbspn'
            },
            {
                name: 'Longanisa Native ',
                carbohydrate: 12,
                protein: 8,
                fat: 10,
                kilocalories: 170,
                kilojoules: 711,
                foodtype: 'meat',
                serving: '3 (2-1/2 cm)'
            }]
    }
}