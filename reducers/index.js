import { combineReducers } from 'redux'
import register from './register'
import app from './app'

export default combineReducers({
    register,
    app,
})