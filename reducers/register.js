import * as types from '../actionTypes/registerActionTypes'

const initialState = {
    user: {
        name: '',
        gender: '',
        age: '',
        weight: '',
        height: '',
        image: '',
        diary: [],
        graph: [],
        bmigraph: [],
        intakes: []
    },
    maleChecked: true,
    femaleChecked: false,
    errors: [],
}

const register = (state = initialState, action) => {
    switch (action.type) {
        case types.UPDATE_NAME:
            return { ...state, user: { ...state.user, name: action.text } }
        case types.CHANGE_GENDER:
            return { ...state, user: { ...state.user, gender: action.gender }, maleChecked: action.maleChecked, femaleChecked: action.femaleChecked }
        case types.UPDATE_AGE:
            return { ...state, user: { ...state.user, age: action.age } }
        case types.UPDATE_WEIGHT:
            return { ...state, user: { ...state.user, weight: action.weight } }
        case types.UPDATE_HEIGHT:
            return { ...state, user: { ...state.user, height: action.height } }
        case types.UPDATE_IMAGE:
            return { ...state, user: { ...state.user, image: action.uri } }
        case types.ERROR:
            return { ...state, errors: state.errors.concat(action.error) }
        case types.SET_USER:
            return { ...state, user: action.user }
        default:
            return state
    }
}

export default register