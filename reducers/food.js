import * as types from '../actionTypes/foodActionTypes'

const initialState = {
    searchText: '',
    foods: [],
    result: [],
    bodyWeightResult: 0
}

const food = (state = initialState, action) => {
    switch (action.type) {
        case types.CHANGE_SEARCH_TEXT:
            return { ...state, searchText: action.text, result: action.result }
        case types.ADD_FOOD:
            return { ...state, foods: state.foods.concat(action.food) }
        case types.CHANGE_FOOD_COUNT:
            return { ...state, foods: state.foods.map((food, index) => index === action.index ? { ...food, count: action.count } : food) }
        case types.REMOVE_FOOD:
            return { ...state, foods: [...state.foods.slice(0, action.index), ...state.foods.slice(action.index + 1)] }
        default:
            return state
    }
}

export default food