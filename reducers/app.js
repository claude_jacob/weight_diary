import * as types from '../actionTypes/appActionTypes'

const initialState = {
    user: null,
    dateCreated: null,
    weightList: [],
    loading: false,
    calories: [],
    lastCalorieUpdate: null,
    total: {
        carbohydrate: 0,
        protein: 0,
        fat: 0,
        kilocalories: 0,
        kilojoules: 0,
    },
    date: new Date(),
    lastBMIUpdate: null
}

const app = (state = initialState, action) => {
    switch (action.type) {
        case types.REGISTER:
            return { ...state, user: action.user }
        case types.UPDATE_USER:
            return { ...state, user: action.user }
        case types.ADD_TO_DIARY:
            return { ...state, user: { ...state.user, diary: state.user.diary.concat(action.content) } }
        case types.REMOVE_DIARY_CONTENT:
            return { ...state, user: { ...state.user, diary: state.user.diary.filter((value, index) => index !== action.index) } }
        case types.ADD_TO_INTAKE:
            return { ...state, user: { ...state.user, intakes: state.user.intakes.concat(action.intake) } }
        case types.REMOVE_INTAKE:
            return { ...state, user: { ...state.user, intakes: state.user.intakes.filter((value, index) => index !== action.index) } }
        case types.UPDATE_HEIGHT_AND_WEIGHT:
            return { ...state, user: { ...state.user, height: action.height, weight: action.weight } }
        case types.UPDATE_BMI:
            return { ...state, user: { ...state.user, bmi: action.bmi } }
        case types.SET_DATE_CREATED:
            return { ...state, dateCreated: action.date }
        case types.UPDATE_WEIGHT_LIST:
            return { ...state, weightList: state.weightList.concat(action.point) }
        case types.ADD_TO_GRAPH:
            return { ...state, user: { ...state.user, graph: state.user.graph.concat(action.data) } }
        case types.UPDATE_LOADING:
            return { ...state, loading: action.value }
        case types.ADD_BMI_TO_GRAPH:
            return { ...state, user: { ...state.user, bmigraph: state.user.bmigraph.concat(action.bmi) } }
        case types.ADD_USER_CALORIE:
            return { ...state, calories: state.calories.concat(action.calorie) }
        case types.SET_USER_CALORIE:
            return { ...state, calories: state.calories.map((cal, i) => i === (state.calories.length - 1) ? action.calorie : cal) }
        case types.RESET_CALORIES:
            return { ...state, calories: [] }
        case types.LAST_CALORIE_UPDATE:
            return { ...state, lastCalorieUpdate: action.date }
        case types.UPDATE_TOTAL:
            return { ...state, total: action.total }
        case types.SET_DATE:
            return { ...state, date: action.date }
        case types.RESET_TOTAL:
            return {
                ...state, total: {
                    carbohydrate: 0,
                    protein: 0,
                    fat: 0,
                    kilocalories: 0,
                    kilojoules: 0,
                }
            }
        case types.LAST_BMI_UPDATE:
            return { ...state, lastBMIUpdate: action.date }
        default:
            return state
    }
}

export default app